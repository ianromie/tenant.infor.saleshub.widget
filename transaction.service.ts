import { Injectable } from "@angular/core";
import { IMIRequest, IMIResponse, AngularCommon } from "./angular-common";

import { AsyncSubject } from "rxjs/AsyncSubject";
import { Observable } from "rxjs/Observable";

export class TransactionService {
    static listMiData(options: IMIRequest, cache = true): Observable<{}> {
        const subject = new AsyncSubject();
        let storageKey = options.transaction;
        storageKey = options.program;
        if (options.record) {
            for (const record in options.record) {
                if (options.record.hasOwnProperty(record)) {
                    storageKey += "-" + options.record[record];
                }
            }
        }
        if (options.transaction.toLowerCase() === "lstfields") {
            // tslint:disable-next-line:no-console
            console.log(TransactionService.caller.name.toString());
        }
        const stored = TransactionService.getStorageItem<{}>(storageKey);
        if (stored) {
            subject.next(stored);
            subject.complete();
        } else {
            options.maxReturnedRecords = 0;
            const miService = AngularCommon.MIServiceInstance();
            // tslint:disable-next-line:no-console

            miService.execute(options).subscribe((response: IMIResponse) => {
                if (cache) {
                    TransactionService.setStorageItem(storageKey, response.items);
                }
                subject.next(response.items);
                subject.complete();
            }, (errorResponse: IMIResponse) => {
                subject.error(errorResponse);
            });
        }
        return subject.asObservable();
    }

    private static getStorageItem<T>(key: string): T {
        const str = sessionStorage.getItem(key);
        const value = JSON.parse(str) as T;
        return value;
    }

    private static setStorageItem(key: string, value: {}): void {
        const stringValue = JSON.stringify(value);
        sessionStorage.setItem(key, stringValue);
    }

}
