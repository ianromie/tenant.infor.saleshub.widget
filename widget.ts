﻿import { IWidgetContext2, IWidgetInstance2 } from "lime";
import { AngularCommon } from "./angular-common";
import { SalesHubViewerContainerComponent, SalesHubViewerModule } from "./main";

// Widget factory function
export const widgetFactory = (context: IWidgetContext2): IWidgetInstance2 => {
	AngularCommon.initialize(context);
	return {
		angularConfig: {
			moduleType: SalesHubViewerModule,
			componentType: SalesHubViewerContainerComponent
		},
		actions: [
			{
				isPrimary: false,
			}, {
				isPrimary: false
			}, {
				isPrimary: false
			}, {
				isPrimary: true,
				standardIconName: "#icon-add"
			}
		]
	};
};
