import {
    CommonModule
} from "@angular/common";

import {
    FormsModule
} from "@angular/forms";

import {
    ChangeDetectionStrategy, ChangeDetectorRef, Component,
    ElementRef, Input, OnInit, Renderer2, ViewChild, ViewEncapsulation
} from "@angular/core";

import {
    ExpandableAreaComponent, SohoBusyIndicatorDirective, SohoComponentsModule, SohoSearchFieldComponent,
    SohoToastService, SohoTreeComponent, SohoTreeService
} from "@infor/sohoxi-angular";

import {
    ArrayUtil, CommonUtil as lmCommonUtil, DialogButtonType, DialogService, IDialogOptions,
    ILanguage, ILaunchOptions, IWidgetAction, IWidgetComponent, IWidgetContext2, IWidgetInstance2,
    IWidgetMessage, IWidgetSettingsComponent, IWidgetSettingsContext2, IWidgetSettingsInstance2, Log,
    NumUtil, StandardDialogButtons, WidgetMessageType, WidgetState
} from "lime";

import {
    Subject
} from "rxjs/Subject";

import {
    AsyncSubject
} from "rxjs/AsyncSubject";

import {
    Observable
} from "rxjs/Observable";

import {
    Bookmark, IBookmark, IMIRequest, IMIResponse, IMIService, IUserContext,
    AngularCommon, MIUtil
} from "./angular-common";

import {
    BookmarkSupport, BookmarkType, ChartType, DataDisplayType, GroupType, IInput,
    IM3ViewerSettings, IOutput, RefreshIntervals
} from "./viewer.interfaces";

import {
    M3CommonUtil
} from "./util";

import {
    TransactionComponent
} from "./transaction.component";

interface IDisplayOption {
    value: DataDisplayType;
    label: string;
}

interface IRefreshOption {
    value: number;
    label: string;
}

/**
 * This component can be used to display a Title setting field with a padlock.
 * It works with the given (required) widgetSettingContext to determine whether title should
 * be locked, unlocked, editable, unlockable etc.
 *
 * Call save() to commit the changes to the widget settings.
 */
@Component({
    selector: "infor-sample-setting-title-field",
    template: `
	<div class="field">
		<label *ngIf="label">{{label}}</label>
        <span class="lookup-wrapper">
            <input [readOnly]="!isTitleEditEnabled || isTitleLocked" [(ngModel)]="title" class="lookup" />
            <button class="inputButton"
                soho-button="icon"
                [icon]="isTitleLocked ? 'locked' : 'unlocked'"
                [disabled]="!isTitleUnlockable"
                (click)="onLockClicked()">
            </button>
        </span>
	</div>
	`,
    styles: [`
        infor-sample-setting-title-field button.inputButton {
            position: absolute;
            top: 0px;
            right: 1px
        }
    `],
    encapsulation: ViewEncapsulation.None

})
export class TitleSettingComponent implements OnInit {
    @Input() widgetSettingsContext: IWidgetSettingsContext2;
    @Input() label: string;

    title: string;
    isTitleEditEnabled: boolean;
    isTitleUnlockable: boolean;
    isTitleLocked: boolean;

    ngOnInit(): void {
        if (!this.widgetSettingsContext) {
            throw new Error("Required input: widgetSettingsContext");
        }

        const widgetContext = this.widgetSettingsContext.getWidgetContext();
        this.isTitleEditEnabled = widgetContext.isTitleEditEnabled();
        this.isTitleLocked = widgetContext.isTitleLocked();
        this.title = widgetContext.getResolvedTitle(this.isTitleLocked);
        this.isTitleUnlockable = widgetContext.isTitleUnlockable();
    }

	/**
	 * Persist changes to the title and lock by saving to widget context.
	 */
    save(): void {
        const widgetContext = this.widgetSettingsContext.getWidgetContext();
        widgetContext.setTitleLocked(this.isTitleLocked);
        if (this.isTitleEditEnabled) {
            widgetContext.setTitle(this.title);
        }
    }

    onLockClicked(): void {
        this.isTitleLocked = !!!this.isTitleLocked;
        if (this.isTitleLocked) {
            const widgetContext = this.widgetSettingsContext.getWidgetContext();
            this.title = widgetContext.getResolvedTitle(this.isTitleLocked);
        }
    }
}
//////////////////////////////
// SettingsComponent
/////////////////////////////
@Component({
    template: `
	<div soho-busyindicator blockUI="true" displayDelay="0">
        <div class="m3-viewer-settings" *ngIf="widgetSettingsContext && settings">
            <infor-sample-setting-title-field
                [widgetSettingsContext]="widgetSettingsContext"
                [label]="translate('title')">
            </infor-sample-setting-title-field>
            <div class="field" *ngIf="refreshRateVisible">
                <label for="infor-m3-viewer-settings-autoRefresh">{{translate("refreshRate")}}</label>
                <select soho-dropdown name="infor-m3-viewer-settings-autoRefresh" [(ngModel)]="settings.refreshRate" [disabled]="!refreshRateEnabled">
                    <option *ngFor="let option of refreshOptions" [ngValue]="option.value">{{option.label}}</option>
                </select>
            </div>

            <!-- <m3-viewer-transaction-directive settings="settings" context="widgetContext" mode="viewer-settings"></m3-viewer-transaction-directive>-->
            <m3-viewer-transaction mode="viewer-settings" [settings]="settings" [widgetContext]="widgetContext" [widgetInstance]="widgetInstance" [miService]="miService"></m3-viewer-transaction>

            <div class="field" *ngIf="logicalId">
                <label for="infor-m3-viewer-settings-application-selector">{{translate("application")}}</label>
                <select soho-dropdown name="infor-m3-viewer-settings-application-selector" [(ngModel)]="logicalId">
                    <option *ngFor="let option of applicationOptions" [ngValue]="option.value">{{option.text}}</option>
                </select>
            </div>
        </div>
	</div>
	`,
})
export class ViewerSettingsComponent implements IWidgetSettingsComponent, OnInit {
    @Input() widgetSettingsContext: IWidgetSettingsContext2;
    @Input() widgetSettingsInstance: IWidgetSettingsInstance2;

    @ViewChild(TitleSettingComponent) titleSettingComponent: TitleSettingComponent;
    @ViewChild(TransactionComponent) transactionComponent: TransactionComponent;
    @ViewChild(SohoBusyIndicatorDirective) busyIndicator: SohoBusyIndicatorDirective;

    settings: IM3ViewerSettings;
    private widgetContext: IWidgetContext2;
    private settingsInstance: IWidgetSettingsInstance2;
    private refreshOptions: IRefreshOption[];
    private applicationOptions: any[] = [];
    private logicalId: string;
    private miService: IMIService;
    private titleEditEnabled: boolean;
    private isTitleUnlockable: boolean;
    private isTitleLocked: boolean;
    private title: string;
    private lang: ILanguage;

    private refreshRateVisible: boolean;
    private refreshRateEnabled: boolean;

    ngOnInit(): void {
        this.widgetContext = this.widgetSettingsContext.getWidgetContext();
        this.lang = this.widgetContext.getLanguage();
        this.miService = AngularCommon.MIServiceInstance();
        this.initFromSettings();
        this.initRefreshRates();
        this.setupSettingsClosingHandler();
    }

    private initFromSettings() {
        const widgetContext = this.widgetSettingsContext.getWidgetContext();
        const settings = widgetContext.getSettings();
        const savedSettings = widgetContext.getSettings().getValues() ? $.extend({}, widgetContext.getSettings().getValues()) : undefined;

        this.refreshRateVisible = settings.isSettingVisible("refreshRate");
        this.refreshRateEnabled = settings.isSettingEnabled("refreshRate");
        if (Object.keys(savedSettings).length) {
            this.settings = $.extend({}, savedSettings);
        } else {
            this.setDefaultValues();
        }
    }

    private setDefaultValues(): void {
        this.settings = {
            refreshRate: 0,
            displayType: DataDisplayType.List,
            program: "",
            transaction: "",
            input: [],
            output: [],
            maxRecords: 15,
            enableBookmarks: false,
            bookmarkData: {},
            chartSettings: {
                chartType: ChartType.Pie,
                labelColumn: "",
                valueColumns: [],
                groupByLabel: false,
                drilldownSettings: {
                    program: "",
                    transaction: "",
                    input: [],
                    output: [],
                    maxRecords: 15
                }
            }
        };
    }

    private onClickIsLocked(): void {
        const isLocked = this.isTitleLocked;
        this.isTitleLocked = !isLocked;
        if (this.isTitleLocked) {
            this.title = this.widgetContext.getResolvedTitle(this.isTitleLocked);
        }
    }

    private createApplicationSelector(): void {
        // Show application instance selector if there are more than one instance
        const context = this.widgetContext;
        const applications = context.getApplications();
        if (!applications || applications.length < 2) {
            return;
        }

        const logicalId = context.getLogicalId() || "";
        this.logicalId = logicalId;

        for (const application of applications) {
            const value = application.logicalId;
            const text = application.productName + " (" + value + ")";
            const item = { text: text, value: value };
            this.applicationOptions.push(item);
        }
    }

    private setupSettingsClosingHandler(): void {
        this.widgetSettingsInstance.closing = (closingArg) => {
            const settings = this.widgetSettingsContext.getWidgetContext().getSettings();
            if (closingArg.isSave) {
                // if (this.refreshRateEnabled) {
                //     settings.set("refreshRate", this.settings.refreshRate);
                // }
                this.titleSettingComponent.save();
                settings.setValues(this.settings);
            }
        };
    }

    private initRefreshRates() {
        const ri = RefreshIntervals;
        this.refreshOptions = [
            {
                value: ri.NO_REFRESH,
                label: this.translate("doNotRefresh")
            },
            {
                value: ri.FIFTEEN_MIN,
                label: ri.FIFTEEN_MIN + " " + this.translate("minutes")
            },
            {
                value: ri.THIRTY_MIN,
                label: ri.THIRTY_MIN + " " + this.translate("minutes")
            },
            {
                value: ri.FORTYFIVE_MIN,
                label: ri.FORTYFIVE_MIN + " " + this.translate("minutes")
            },
            {
                value: ri.SIXTY_MIN,
                label: ri.SIXTY_MIN + " " + this.translate("minutes")
            }];
    }
    private translate(text: string): string {
        return this.lang ? this.lang.get(text) : "";
    }
}
