﻿import { HttpClient, HttpHandler, HttpXhrBackend } from "@angular/common/http";
import { Injector, ContentChild } from "@angular/core";
import { ArrayUtil, CommonUtil, IIonApiRequestOptions, IIonApiResponse, IWidgetContext2, StringUtil } from "lime";
import { AsyncSubject } from "rxjs/AsyncSubject";
import { Observable } from "rxjs/Observable";
import { stringify } from "@angular/core/src/util";

/**
 * Defines user context values for an M3 user.
 */
export interface IUserContext {
	// ReSharper disable InconsistentNaming

	/**
	 * Company.
	 */
	CONO?: string;

	/**
	 * Division.
	 */
	DIVI?: string;

	/**
	 * Language.
	 */
	LANC?: string;

	/**
	 * Date format.
	 */
	DTFM?: string;

	/**
	 * Decimal format.
	 */
	DCFM?: string;

	/**
	 * Time zone.
	 */
	TIZO?: string;

	/**
	 * Facility.
	 */
	FACI?: string;

	/**
	 * Warehouse.
	 */
	WHLO?: string;

	/**
	 * Company name.
	 */
	TX40?: string;

	/**
	 * Division name.
	 */
	CONM?: string;

	/**
	 * Start menu.
	 */
	DFMN?: string;

	/**
	 * User.
	 */
	USID?: string;

	/**
	 * Name.
	 */
	NAME?: string;

	/**
	 * User status.
	 */
	USTA?: string;

	/**
	 * User type.
	 */
	USTP?: string;

	/**
	 * Equiment alias search sequense.
	 */
	EQAL?: string;

	/**
	 * Electronic mail address.
	 */
	EMAL?: string;

	MNVR?: string;
	// ReSharper restore InconsistentNaming
}

/**
 * The possible data types for a MI value: String, Numeric or Date.
 */
// ReSharper disable once InconsistentNaming
export enum MIDataType {
	/**
	 * The data type is a string.
	 */
	String,
	/**
	 * The data type is a numeric value.
	 */
	Numeric,
	/**
	 * The data type is a date.
	 */
	Date
}

/**
 * Name and value representation that is returned by the M3 API for each field name / value.
 * @internal
 */
export interface INameValue {
	/**
	 * The name of the value.
	 */
	name: string;
	/**
	 * The value.
	 */
	value: string;
}

/**
 * Constants used by MIAccess framework.
 */
// ReSharper disable once InconsistentNaming
export class MIConstants {
	/**
	 * Gets or sets the database format for M3 API transactions. The format is ALWAYS yyyyMMdd.
	 */
	static datePattern = "yyyyMMdd";
	/**
	 * Gets or sets the decimal separator for M3 API values. The separator is always dot for numeric values.
	 */
	static decimalSeparator = ".";
}

/**
 * The meta data definition for a M3 API field to determine the type.
 */
// ReSharper disable once InconsistentNaming
export interface IMIMetadataInfo {
	/**
	 * Gets or sets the name if the field.
	 */
	name: string;

	/**
	 * Gets or sets the fields type. There are three type String, Numeric and Date.
	 */
	type: MIDataType;

	/**
	 * Gets or sets the maximal length of this field in the API transaction.
	 */
	length: number;

	/**
	 * Gets or sets the description of the field.
	 */
	description: string;

	/**
	 * Gets a value that indicates if the field is defined as a numeric type.
	 */
	isNumeric(): boolean;

	/**
	 * Gets a value that indicates if the field is defined as a date type.
	 */
	isDate(): boolean;

	/**
	 * Gets a value that indicates if the field is defined as a string type.
	 */
	isString(): boolean;
}

/**
 * Interface that represents a dictionary structure where the name of the field is the key and the value is the meta data
 * associated with that field.
 */
// ReSharper disable once InconsistentNaming
export interface IMIMetadataMap {
	/**
	 * Map with meta data info.
	 * @param name The name of the field. For example ITNO.
	 * @returns The meta data info for that field.
	 */
	[name: string]: IMIMetadataInfo;
}

/**
 * Options for executing a [[IMIRequest]].
 */
// ReSharper disable once InconsistentNaming
export interface IMIOptions {
	/**
	 * Gets or sets the company to use for the request.
	 * If this value is set it will override the company from all other sources.
	 */
	company?: string;

	/**
	 * Gets or sets the division to use for the request.
	 * If this value is set it will override the division from all other sources.
	 */
	division?: string;

	/**
	 * Gets or sets a value that indicates if empty values should be returned from the server. Default is false.
	 */
	excludeEmptyValues?: boolean;

	/**
	 * Gets or sets the maximum number of records to return.
	 * Always check maxReturnedRecordsFieldSpecified before using this value since 0 means that all rows should be returned.
	 */
	maxReturnedRecords?: number;

	/**
	 * Gets or sets an arbitrary object value that can be used to store custom information about this request.
	 * The value will be returned in the tag property of the MIReponse object.
	 */
	tag?: any;

	/**
	 * Gets or sets if metadata should be included as part of the reply. Default is false.
	 */
	includeMetadata?: boolean;

	/**
	* Gets or sets if output should be converted to numbers and dates according to the meta data definition for the MI transaction.
	* This implicitly turns on includeMetadata in the options to load the meta data information. Default is false.
	*/
	typedOutput?: boolean;

	// TODO automatically store original values so that the same record can be used for binding and to see what values has been changed
	//keepOriginalCopy
}

/**
 * Request used to execute M3 MI transactions. It is recommended to set the [[IMIRequest.outputFields]].
 *
 * **Example**
 *
 *				var record = { USID: "MVXSECOFR" };
 *	var output = ["USID","NAME","CONO","DIVI"];
 *	var request = {
 * 		program: "MNS150",
 *		transaction: "GetUserData",
 * 		record: record,
 *		outputFields: output
 *	};
 *
 * You can also use the [[MIRecord]] class to set input data to get date or numeric values converted to the correct format.
 *
 * **Example**
 *
 *				var record = new MIRecord();
 *	var today = new Date();
 *	record.setDateString("LSTD", today);
 *
 *	var quantity = 1.5;
 *	record.setNumberString("QQTY", quantity);
 *
 */
// ReSharper disable once InconsistentNaming
export interface IMIRequest extends IMIOptions {
	/**
	 * Gets or sets the base URL of the M3 MI REST service.
	 */
	baseUrl?: string;

	/**
	 * Gets or sets the name of the MI program.
	 */
	program?: string;

	/**
	 * Gets or sets the name of the transaction.
	 */
	transaction?: string;

	/**
	* Gets or sets the MIRecord containing the input data to the transaction.
	* This property is not required for transactions without mandatory input fields.
	*/
	record?: any;

	/**
	 * Gets or sets an array with the names of the output fields to return from a transaction.
	 *
	 * Use this property to limit the amount of data to transfer from the server.
	 * Only specify the names of the fields that will actually be used since that will approve the performance.
	 */
	outputFields?: string[];
}

/**
 * Represents the response from an M3 MI transaction. The response is a single MIRecord or a list of MIRecords
 * but they are types as any to support the syntax in the example below.
 *
 * **Example**
 *
 *			public onResponse(response: M3.IMIResponse) {
 *		if(response.item){
 *			var name = response.item.NAME;
 *		}
 *	}
 *
 * If the request has [[IMIRequest.typedOutput]] set to true all values in the response will be typed according
 * to the [[MIDataType]], (String, Numeric and Date). The default is that all values are strings.
 */
// ReSharper disable once InconsistentNaming
export interface IMIResponse {
	/**
	 * Gets or sets an error.
	*/
	error?: any;

	/**
	 * Gets or sets an error message.
	 */
	errorMessage?: string;

	/**
	 * Gets or sets an error code.
	 */
	errorCode?: string;

	/**
	 * Gets a value that indicates if an error exists in the response.
	 *
	 * An error is considered to exist if any of the error, errorMessage or errorCode properties are set.
	 * Note that if one property is set there is no guarantee that any of the other properties are set.
	 *
	 * @returns True if an error exists.
	 */
	hasError(): boolean;

	/**
	 * Gets or sets the name of the MI program.
	 */
	program?: string;

	/**
	 * Gets or sets the name of the transaction.
	 */
	transaction?: string;

	/**
	* Gets the first item in the items list. The item is a [[MIRecord]].
	*/
	item?: any;

	/**
	 * Gets or sets a list or MIRecords returned from the transaction. See [[MIRecord]].
	 */
	items?: any[];

	/**
	 * Gets or sets the transaction field that is the cause of the error.
	 */
	errorField?: string;

	/**
	 * Gets or sets the error type.
	 */
	errorType?: string;

	/**
	 * Metadata describing the values and their data types. To get the metadata set [[IMIOptions.includeMetadata]]
	 */
	metadata: IMIMetadataMap;

	/**
	 * Gets an arbitrary object value that was set on the tag property on the IMIRequest object.
	 */
	tag?: any;
}

/**
 * Represents the response from an M3 MI transaction. See [[IMIResponse]].
 * @internal
 */
// ReSharper disable once InconsistentNaming
export class MIResponse implements IMIResponse {
	program: string = null;
	transaction: string = null;
	tag: any;
	item: MIRecord;
	items: MIRecord[];
	errorField: string;
	errorType: string;
	error: any;
	errorMessage: string;
	errorCode: string;
	metadata: IMIMetadataMap;

	hasError(): boolean {
		const state = this;
		return !!(state.errorMessage || state.errorCode || state.error);
	}
}

export class ShubResponse {
	Program: string = null;
	Transaction: string = null;
	MetaData: ShubMetaObject = null;
	MIRecord: any = {};
}
export class ShubRecord {
	NameValue: ShubNameValueObject[] = null;
	RowIndex: number = 0;
}
export class ShubNameValueObject {
	Name: string = "";
	Value: string = "";
}
export class ShubMetaObject {
	Field: any = [];
}

/**
 * Executes M3 MI transactions. See [[IMIService]].
 * @internal
 */
// ReSharper disable once InconsistentNaming
export class MIAccess {
	getDefaultBaseUrl() {
		return "sh_rs";
	}

	createUrl(baseUrl: string, request: IMIRequest): string {
		let url = baseUrl + "/" + request.program + "/" + request.transaction;

		let maxRecords = 100;
		let excludeEmpty = "true";
		let metadata = "true";
		let returnCols = null;
		if (request.maxReturnedRecords >= 0) {
			maxRecords = request.maxReturnedRecords;
		}
		if (!request.excludeEmptyValues) {
			excludeEmpty = "false";
		}
		if (request.outputFields && request.outputFields.length > 0) {
			returnCols = request.outputFields.join(",");
		}

		if (request.includeMetadata) {
			metadata = "true";
			request.includeMetadata = true;
		} else {
			request.includeMetadata = false;
		}

		// Add "mandatory" parameters
		//url += ";metadata=" + metadata + ";maxrecs=" + maxRecords + ";excludempty=" + excludeEmpty;

		const company = request.company;
		if (company) {
			//url += ";cono=" + company;

			const division = request.division;
			if (division || division === "") {
				//	url += ";divi=" + division;
			}
		}

		// Add optional parameters
		if (returnCols) {
			//	url += ";returncols=" + returnCols;
		}


		const trans = this.manipulateTransaction(request);

		let prog = request.program;
		if (request.record) {
			if (request.record.TRTP) {
				prog = (request.record.TRTP.toLowerCase() === "i") ? "swagger.json" : request.program;
			}
		}


		url = baseUrl + "/" + prog + trans.trim();

		let recordAdded = false;
		const record = request.record;
		if (record) {

			for (const field in record) {
				if (record.hasOwnProperty(field)) {
					const value = record[field];
					if (value != null) {

						const params = "{" + field + "}";
						const exist = url.search(params);

						if (exist >= 0) {
							url = url.replace(params, value);
						} else {
							url += "?";
							url += (recordAdded ? "&" : "") + field + "=" + encodeURIComponent(value);
							if (!recordAdded) {
								recordAdded = true;
							}
						}


					}
				}
			}
		}

		// TODO There should be an options that controls if the request ID
		//url += (recordAdded ? "&_rid=" : "_rid=") + CommonUtil.random();

		//url = baseUrl + "/" + request.program + "/" + request.transaction;
		// tslint:disable-next-line:no-console
		// tslint:disable-next-line:no-console
		return url;
	}

	private manipulateTransaction(request: IMIRequest) {
		let transaction: string = "";
		const tran = request.transaction;
		const prog = request.program;
		const constanttransaction: any = ["transactions", "programs", "LstIFields"];

		const inArrayIndex = $.inArray(tran, constanttransaction);


		if (inArrayIndex >= 0) {
			if (tran === "LstIFields") {

				if (request.record.TRTP.toLowerCase() === "i") {
					transaction = "";
				} else {
					transaction = "/" + request.record.MINM.trim();
				}

			}
		} else {
			transaction = tran.split(prog)[1];

		}

		return transaction;

	}

	private formatShubResponse(request: IMIRequest, content: any): ShubResponse {

		const shubResponse = new ShubResponse();
		shubResponse.Transaction = request.transaction;
		shubResponse.Program = request.program;

		const params = request.outputFields;

		const metadataField: any = [];


		params.forEach(element => {
			metadataField.push({
				"@description": "",
				"@length": 0,
				"@name": element,
				"@type": "",
			});
		});

		const metadata = new ShubMetaObject();
		metadata.Field = metadataField;

		shubResponse.MetaData = metadata;

		shubResponse.MIRecord = this.formatShubRecord(shubResponse, content, request);

		return shubResponse;

	}

	private toShubRecord(smocontainer: ShubRecord[], ret: ShubResponse, content: any): ShubRecord[] {

		const sbrecord = (content.paths) as SbRecord;

		const records = content.tags;

		if (records == null || records.length < 1) {
			// An empty response
			return;
		}

		// tslint:disable-next-line:prefer-for-of
		for (let i = 0; i < records.length; i++) {
			const smo = new ShubRecord();
			const record: any = records[i];

			if (record != null) {

				const name: string = record.name;
				const trim = name.trim().split(":")[0];
				const conc: string = "/" + trim.trim().toString();

				const currentrec = sbrecord[conc.toString()];

				if (currentrec != null) {

					smo.RowIndex = i;

					const nmvcontainer: ShubNameValueObject[] = [];
					ret.MetaData.Field.forEach((e: { [x: string]: string; }) => {

						const nmv = new ShubNameValueObject();
						nmv.Name = e["@name"].toString().trim();

						nmv.Value = trim.trim();

						nmvcontainer.push(nmv);

					});

					smo.NameValue = nmvcontainer;
					smocontainer.push(smo);
				}

			}

		}
		return smocontainer;
	}

	private formatToTransactionShubRecord(smocontainer: ShubRecord[], ret: ShubResponse, content: any, request: IMIRequest): ShubRecord[] {

		const sbrecordd = (content.paths) as SbRecord;

		const prog = request.record.MINM.split("-")[0].trim();

		const pro = $.map(Object.keys(sbrecordd), (q, w) => {

			if (q.trim().toString().startsWith("/" + prog.trim())) { sbrecordd[q.trim().toString()].pathname = q.trim().toString(); return sbrecordd[q.trim().toString()]; }
		});

		//tslint:disable-next-line:prefer-for-of
		for (let i = 0; i < pro.length; i++) {

			const smo = new ShubRecord();

			const record: any = pro[i];

			if (record != null) {

				if (record.get) {


					smo.RowIndex = i;

					const nmvcontainer: ShubNameValueObject[] = [];
					ret.MetaData.Field.forEach((e: { [x: string]: string; }) => {

						const nmv = new ShubNameValueObject();
						nmv.Name = e["@name"].toString().trim();

						//	nmv.Value = record.get[e["@name"]].toString();
						nmv.Value = (e["@name"] === "operationId") ? record.pathname : record.get[e["@name"]].toString();
						nmvcontainer.push(nmv);

					});

					smo.NameValue = nmvcontainer;
					smocontainer.push(smo);

				}
			}
		}
		return smocontainer;
	}
	private listShubFields(smocontainer: ShubRecord[], ret: ShubResponse, content: any, request: IMIRequest): ShubRecord[] {
		if (request.record.TRTP.toLowerCase() === "i") {
			smocontainer = this.getInputField(smocontainer, ret, content, request);
		} else if (request.record.TRTP.toLowerCase() === "o") {
			smocontainer = this.getOutputField(smocontainer, ret, content, request);
		}
		return smocontainer;
	}

	private sortInputOutput(params: any, keyToSort: string): any {

		function sortByKey(a: any, b: any) {
			const x = a[keyToSort];
			const y = b[keyToSort];
			return ((x < y) ? -1 : ((x > y) ? 1 : 0));
		}
		return params.sort(sortByKey);
	}

	private getOutputField(smocontainer: ShubRecord[], ret: ShubResponse, content: any, request: IMIRequest): ShubRecord[] {

		if (content.fields) {
			content.fields = this.sortInputOutput(content.fields, "name");

			for (let i = 0; i < content.fields.length; i++) {
				const e = content.fields[i];
				const smo = new ShubRecord();
				smo.RowIndex = i;

				const nmvcontainer: ShubNameValueObject[] = [];
				ret.MetaData.Field.forEach((element: any) => {
					const nmv = new ShubNameValueObject();
					const fieldname = element["@name"].toString().trim();
					nmv.Name = fieldname;
					switch (fieldname) {
						case "FLNM": {
							nmv.Value = e.name;
							break;
						}
						case "FLDS": {
							nmv.Value = e.name;
							break;
						} case "TYPE": {
							//nmv.Value = e.schema.type;
							nmv.Value = e.dataType;
							break;
						} case "MAND": {
							nmv.Value = "0";
							break;
						}
					}
					nmvcontainer.push(nmv);
				});

				smo.NameValue = nmvcontainer;
				smocontainer.push(smo);
			}
		}
		return smocontainer;
	}
	private getInputField(smocontainer: ShubRecord[], ret: ShubResponse, content: any, request: IMIRequest): ShubRecord[] {

		const sbrecordd = (content.paths) as SbRecord;
		const prog = request.record.MINM.split("-")[0].trim();

		const p = "/" + prog.trim() + request.record.TRNM.trim();

		const record = sbrecordd[request.record.TRNM.trim()];

		if (record != null) {

			if (record.get) {

				if (record.get.parameters != null) {

					let params = record.get.parameters;
					params = this.sortInputOutput(params, "name");
					let ii: number = 0;
					params.forEach((e: any) => {
						const smo = new ShubRecord();
						smo.RowIndex = ii;

						const nmvcontainer: ShubNameValueObject[] = [];


						ret.MetaData.Field.forEach((element: any) => {
							const nmv = new ShubNameValueObject();
							const fieldname = element["@name"].toString().trim();
							nmv.Name = fieldname;
							switch (fieldname) {
								case "FLNM": {
									nmv.Value = e.name;
									break;
								}
								case "FLDS": {
									nmv.Value = (e.description) ? e.description : e.name;
									break;
								} case "TYPE": {
									//nmv.Value = e.schema.type;
									nmv.Value = "A";
									break;
								} case "MAND": {
									nmv.Value = (e.required) ? "1" : "0";
									break;
								}
							}
							nmvcontainer.push(nmv);
						});
						ii++;
						smo.NameValue = nmvcontainer;
						smocontainer.push(smo);
					});

				}
			}
		}
		// tslint:disable-next-line:no-console
		console.log(smocontainer);
		return smocontainer;
	}

	private DataSwagger(smocontainer: ShubRecord[], ret: ShubResponse, content: any, request: IMIRequest): ShubRecord[] {

		const records = (!Array.isArray(content)) ? [content] : content;

		// tslint:disable-next-line:no-console
		//console.log(content); console.log(smocontainer); console.log(ret);
		// // tslint:disable-next-line:prefer-for-of
		for (let i = 0; i < records.length; i++) {
			const smo = new ShubRecord();
			const record: any = records[i];

			if (record != null) {

				smo.RowIndex = i;

				const nmvcontainer: ShubNameValueObject[] = [];

				ret.MetaData.Field.forEach((e: any) => {

					const nmv = new ShubNameValueObject();
					nmv.Name = e["@name"].toString().trim();


					if (record[e["@name"]]) {
						nmv.Value = record[e["@name"]].toString();
					} else {
						nmv.Value = "";
					}


					nmvcontainer.push(nmv);

				});

				smo.NameValue = nmvcontainer;
				smocontainer.push(smo);
			}
		}
		return smocontainer;
	}

	private formatShubRecord(res: ShubResponse, content: any, request: IMIRequest): ShubRecord[] {

		let smocontainer: ShubRecord[] = [];
		if (res.Transaction.toLocaleLowerCase() === "programs") {
			smocontainer = this.toShubRecord(smocontainer, res, content);

		} else if (res.Transaction.toLocaleLowerCase() === "transactions") {
			smocontainer = this.formatToTransactionShubRecord(smocontainer, res, content, request);
		} else if (res.Transaction.toLocaleLowerCase() === "lstifields") {

			smocontainer = this.listShubFields(smocontainer, res, content, request);
		} else {
			smocontainer = this.DataSwagger(smocontainer, res, content, request);
		}

		return smocontainer;
	}
	private parseMessage(response: IMIResponse, content: any) {
		const code = content["@code"];
		const field = content["@field"];
		const errorType = content["@type"];
		let message = content.Message;
		if (message) {
			// Clean up the message that might contain the code, field and a lot of whitespace.
			if (code) {
				message = message.replace(code, "");
			}
			if (field) {
				message = message.replace(field, "");
			}
			response.errorMessage = message.trim();
		}
		response.errorCode = code ? code : errorType;
		response.errorField = field;
		response.errorType = content["@type"];
	}

	// tslint:disable-next-line:member-ordering
	parseResponse(request: IMIRequest, content: any): IMIResponse {
		content = this.formatShubResponse(request, content);

		const response: IMIResponse = new MIResponse();
		response.tag = request.tag;

		const items: any = [];
		response.items = items;
		response.program = request.program;
		response.transaction = request.transaction;

		this.parseMessage(response, content);

		let metadata: IMIMetadataMap = null;
		if (request.includeMetadata) {
			metadata = this.getMetadata(content);
			response.metadata = metadata;
		}

		const records = content.MIRecord;
		if (records == null || records.length < 1) {
			// An empty response
			return response;
		}

		const isTypedOutput = request.typedOutput;

		// tslint:disable-next-line:prefer-for-of
		for (let i = 0; i < records.length; i++) {
			const record: any = records[i];
			if (record != null) {
				const miRecord = new MIRecord();
				miRecord.metadata = metadata;

				if (record.NameValue) {
					// tslint:disable-next-line:prefer-for-of
					for (let index = 0; index < record.NameValue.length; index++) {
						const nameValue = record.NameValue[index];
						const name: string = nameValue.Name;
						let value: string = nameValue.Value;
						if (value != null) {
							value = StringUtil.trimEnd(value);
						}
						if (isTypedOutput) {
							miRecord[name] = this.getTypedValue(name, value, metadata);
						} else {
							miRecord[name] = value;
						}
					}
				}
				items.push(miRecord);
			}
		}

		if (items.length > 0) {
			response.item = items[0];
		}
		return response;
	}

	// tslint:disable-next-line:member-ordering
	parseResponse1(request: IMIRequest, content: any): IMIResponse {
		const response: IMIResponse = new MIResponse();
		response.tag = request.tag;

		const items: any = [];
		response.items = items;
		response.program = request.program;
		response.transaction = request.transaction;
		const so = this.formatShubResponse(request, content);

		this.parseMessage(response, content);

		let metadata: IMIMetadataMap = null;
		if (request.includeMetadata) {

			metadata = this.getMetadata(content);
			response.metadata = metadata;
		}

		const sbrecord = (content.paths) as SbRecord;
		if (response.transaction === "programs") {
			const records = content.tags;
			if (records == null || records.length < 1) {
				// An empty response
				return response;
			}
			const prop = Object.keys(content.paths);
			const isTypedOutput = request.typedOutput;

			// tslint:disable-next-line:prefer-for-of
			for (let i = 0; i < records.length; i++) {

				const record: any = records[i];

				if (record != null) {

					const name: string = record.name;
					const trim = name.trim().split(":")[0];
					const conc: string = "/" + trim.trim().toString();

					const currentrec = sbrecord[conc.toString()];

					if (currentrec != null) {
						const miRecord = new MIRecord();
						miRecord.metadata = metadata;

						const opid: string = "operationId";
						// const opval: string = trim + "-" + currentrec.get.operationId.toString();
						const opval: string = trim.trim();
						miRecord[opid] = opval;
						const opidd: string = "description";
						const opvald: string = trim.trim().toString();
						miRecord[opidd] = opvald;
						items.push(miRecord);

					}
					//	miRecord[currentrec.get.operationId.toString()] = sbrecord[conc.toString()].get.description.toString();

					// tslint:disable-next-line:only-arrow-functions

					// if (record.NameValue) {
					// 	// tslint:disable-next-line:prefer-for-of
					// 	for (let index = 0; index < record.NameValue.length; index++) {
					// 		const nameValue = record.NameValue[index];
					// 		const name: string = nameValue.Name;
					// 		let value: string = nameValue.Value;
					// 		if (value != null) {
					// 			value = StringUtil.trimEnd(value);
					// 		}
					// 		if (isTypedOutput) {
					// 			miRecord[name] = this.getTypedValue(name, value, metadata);
					// 		} else {
					// 			miRecord[name] = value;
					// 		}
					// 	}
					// }
				}

			}
		} else if (response.transaction === "transactions") {
			const sbrecordd = (content.paths) as SbRecord;

			const prog = request.record.MINM.split("-")[0].trim();

			const pro = $.map(Object.keys(sbrecordd), (q, w) => {

				if (q.trim().toString().startsWith("/" + prog.trim())) { return sbrecordd[q.trim().toString()]; }
			});

			//tslint:disable-next-line:prefer-for-of
			for (let i = 0; i < pro.length; i++) {

				const record: any = pro[i];

				if (record != null) {

					if (record.get) {
						const miRecord = new MIRecord();
						miRecord.metadata = metadata;

						const opid: string = "operationId";
						const opval: string = record.get.operationId.toString();
						miRecord[opid] = opval;
						const opidd: string = "description";
						const opvald: string = record.get.description.trim().toString();
						miRecord[opidd] = opvald;
						items.push(miRecord);

					}
				}
			}
		}
		// else if (response.transaction === "LstIFields") {		}

		if (items.length > 0) {
			// tslint:disable-next-line:no-console

			response.item = items[0];
		}

		return response;
	}

	private getTypedValue(name: string, value: string, metadata: { [k: string]: IMIMetadataInfo }): any {
		try {
			if (metadata) {
				const metaDataInfo = metadata[name];
				if (!metaDataInfo) {
					return value;
				}

				const result = this.parseValue(value, metaDataInfo);
				return result;
			} else {
				return value;
			}
		} catch (e) {
			// TODO log but only once per field / transaction / program
			return value;
		}
	}

	private parseValue(value: string, metadataInfo: IMIMetadataInfo): any {
		if (metadataInfo.isString()) {
			return value;
		}
		if (metadataInfo.isNumeric()) {
			if (value) {
				return 0;
			}
			return +value;
		}
		if (metadataInfo.isDate()) {
			if (!value) {
				return null;
			}
			return MIUtil.getDate(value);
		}
		return value;
	}

	private getMetadata(content: any): IMIMetadataMap {
		try {
			const input = content.Metadata;
			if (input && input.Field && input.Field.length > 1) {
				const metadataMap: IMIMetadataMap = {};
				const fields = input.Field;
				for (const record in fields) {
					if (fields.hasOwnProperty(record)) {
						const entry = input.Field[record];
						const name = entry["@name"];
						const metaDataInfo = new MIMetadataInfo(name, entry["@length"], entry["@type"], entry["@description"]);
						metadataMap[name] = metaDataInfo;
					}
				}
				return metadataMap;
			}
		} catch (e) {
			// TODO Support some kind of logger injection for logging.
		}
		return null;
	}
}

export class SbRecord {
	[key: string]: any;
	get: SbRecordGet;

}
export class SbRecordGet {
	description: string;
	operationId: string;
}

/**
 * Represents an input/output record and is used when calling M3 MI transactions. This class can be used when creating an
 * input record with the correct format.
 *
 * In output the MIRecord can contain typed data if [[IMIOptions.typedOutput]] was set to true in the request.
 *
 * In Odin APIs the MIRecord is often defined as <i>any</i> to support the syntax below.
 *
 * **Example**
 *
 *			var record = { USID: "MVXSECOFR", STAT: "20" };
 *
 * When used as input it is important to set the correct format for the API transaction. Using the MIRecord functions
 * for numeric and date will store strings in the correct format.
 *
 * **Example**
 *
 *				// Help with format conversion
 *	var miRecord = new MIRecord();
 *	var today = new Date();
 *	miRecord.setDateString("ORDT", today);
 */
// ReSharper disable once InconsistentNaming
export class MIRecord {
	[key: string]: any;

	constructor(values?: any) {
		if (values) {
			//angular.extend(this, values);
			$.extend(this, values);
		}
	}

	/**
	 * Gets or sets a collection of metadata objects.
	 * To get metdata the [IMIRequest.includemetadata](..\interfaces\m3.imirequest.html#includemetadata) has to be set to true.
	 */
	metadata: IMIMetadataMap = null;

	/**
	 * Sets a number by converting it to a string using the dot as decimal separator.
	 * @param name The name of the field.
	 * @param value The value.
	 */
	setNumberString(name: string, value: number): void {
		this[name] = value.toString(); // Formatting with . separator is default
	}

	/**
	 * Sets a number string. The value must use dot as decimal separator since that is the supported format for M3 API transactions.
	 * @param name The name of the field.
	 * @param value The value.
	 */
	setNumber(name: string, value: string): void {
		this[name] = value;
	}

	/**
	 * Sets a date and transforms it to a string using the date format for M3 API transactions (yyyyMMdd).
	 * @param name The name of the field.
	 * @param value The value.
	 */
	setDateString(name: string, value: Date): void {
		this[name] = MIUtil.getDateFormatted(value);
	}

	/**
	 * Sets the date as a Date.
	 * @param name The name of the field.
	 * @param value The value.
	 */
	setDate(name: string, value: Date): void {
		this[name] = value;
	}

	/**
	 * Sets a string value.
	 * @param name The name of the field.
	 * @param value The value.
	 */
	setString(name: string, value: string): void {
		this[name] = value;
	}
}

/**
 * Util class for MI related functions.
 */
// ReSharper disable once InconsistentNaming
export class MIUtil {

	static isDate(date: any): boolean {
		return date instanceof Date && !isNaN(date.valueOf());
	}

	/**
	 * Converts a date or number to a valid MI Format. Note that strings will not be converted.
	 * @param The value, a date, string or number. Note that the value has to be a Date object for dates and a number for numbers.
	 * @returns A string in MI format.
	 */
	static toMIFormat(value: any): string {
		if (!CommonUtil.hasValue(value)) {
			return "";
		}
		if (MIUtil.isDate(value)) {
			return MIUtil.getDateFormatted(value);
		} else {
			return value.toString(); // Default separator for numbers are .
		}
	}

	/**
	 * Creates a MIRecord that contains only the updated fields and the mandatory keys.
	 * Should be used when making an update transaction so that unchanged values are not set by the update transaction.
	 * Note that the values must have the correct type. Dates must be date objects and numeric values must be numbers.
	 * @param originalValues A [[MIRecord]] with the original values, for example the values from a Get or List transaction.
	 * @param newRecord A MIRecord or object with parameters for all values using the correct data type for the transaction.
	 * @param fieldNames An array with field names that should be updated (if the value has changed). This list does not need to contain the mandatory fields.
	 * @param mandatoryFields An array with mandatoryFields. The mandatory fields should always be added to the new record.
	 * @returns A [[MIRecord]] that should be used in an update transaction.
	 */
	static createUpdateRecord(originalValues: MIRecord, newRecord: any, fieldNames: string[], mandatoryFields: string[]): MIRecord {

		const updateRecord = new MIRecord();
		//const allFields = angular.copy(fieldNames);
		const allFields = $.merge([], fieldNames);

		if (mandatoryFields != null && mandatoryFields.length > 0) {
			for (const mandatoryField of mandatoryFields) {
				if (!(ArrayUtil.contains(allFields, mandatoryField))) {
					allFields.push(mandatoryField);
				}
			}
		}
		allFields.forEach((field: any, index: any) => {
			const oldValue = originalValues[field];
			const newValue = newRecord[field];
			// Always include mandatory fields
			if (ArrayUtil.contains(mandatoryFields, field)) {
				if (CommonUtil.hasValue(newValue)) {
					updateRecord[field] = MIUtil.toMIFormat(newValue);
				} else {
					updateRecord[field] = MIUtil.toMIFormat(oldValue);
				}
			} else if (CommonUtil.hasValue(newValue) && newValue !== oldValue) {
				updateRecord[field] = newValue;
			}
		});
		return updateRecord;
	}

	/**
	* Returns a string formatted in the database format yyyyMMdd which is the format used in all M3 API transactions.
	* @param date A date object.
	* @returns A string representaion of the date in yyyyMMdd format.
	*/
	static getDateFormatted(date: Date): string {
		const yyyy = date.getFullYear().toString();
		const mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
		const dd = date.getDate().toString();
		return yyyy + (mm.length === 2 ? mm : "0" + mm[0]) + (dd.length === 2 ? dd : "0" + dd[0]); // padding
	}

	/**
	 * Converts a string in the database format yyyMMdd to a Date object. This method will throw an exception.
	 * @param yyyymmdd A date in database format.
	 * @returns A date object.
	 */
	static getDate(yyyymmdd: string): Date {
		const dateString = yyyymmdd;
		const year: number = +dateString.substring(0, 4);
		const month: number = +dateString.substring(4, 6);
		const day: number = +dateString.substring(6, 8);

		// Month is zero based
		return new Date(year, month - 1, day);
	}

	/**
	 * Converts a dictionary structure to an array.
	 * @param metdataMap A map structure where the key is the field name and the value is a [[MIMetadataInfo]].
	 * @returns An array with [[MIMetadataInfo]].
	 *
	 */
	static metadataToArray(metadataMap: IMIMetadataMap): IMIMetadataInfo[] {
		const array = new Array();
		for (const field in metadataMap) {
			if (metadataMap.hasOwnProperty(field)) {
				const metadata = metadataMap[field];
				array.push(metadata);
			}
		}
		return array;
	}
}

/**
 * Implementation of [[IMIMetadataInfo]].
 * @internal
 */
// ReSharper disable once InconsistentNaming
export class MIMetadataInfo implements IMIMetadataInfo {
	name: string;
	type: MIDataType;
	length: number;
	description: string;

	constructor(name: string, length: number, typeString: string, description: string) {
		this.name = name;
		this.length = length;
		this.description = description;
		this.setType(typeString);
	}

	isNumeric(): boolean {
		return this.type === MIDataType.Numeric;
	}

	isDate(): boolean {
		return this.type === MIDataType.Date;
	}
	isString(): boolean {
		return this.type === MIDataType.String;
	}

	private setType(value: string) {
		if (value === "D") {
			this.type = MIDataType.Date;
		} else if (value === "N") {
			this.type = MIDataType.Numeric;
		} else if (value === "A") {
			this.type = MIDataType.String;
		}
	}
}

/**
 * Service for calling M3 MI programs.
 *
 * Angular service name: "m3MIService".
 *
 * If you want to access MI programs from within your controller it is recommended to extend [[MIListCtrl]] or [[MIDetailCtrl]]
 * to get busy and active handling.
 *
 * The service is used to call M3 MI programs. Below is an example that requires input.
 *
 *  **Example**
 *
 *				var outputFields = ["USID","NAME","CONO","DIVI"];
 *	var record = { USID:"MVXSECOFR" };
 *	var request: M3.IMIRequest = {
 *		program: "MNS150MI",
 *		transaction: "GetUserData",
 *		record: record,
 *		outputFields: outputFields
 *	};
 *
 *	// This is the service call
 *		miService.executeRequest(request).then(
 *		(response: M3.IMIResponse) => {
 *			// Handle the response
 *		}, (response: M3.IMIResponse) => {
 *			// Handle the error
 *			alert(response.errorMessage);
 *		});
 *
 */
// ReSharper disable once InconsistentNaming
export interface IMIService {
	/**
	 * Executes a MI transaction.
	 *
	 * @param request The IMIRequest that contains input and information about the request.
	 * @returns A promise that will resolve a IMIResponse. If the promise is rejected the IMIResponse will contain some error information.
	 */
	execute(request: IMIRequest): Observable<IMIResponse>;

	/**
	 * Gets the M3 user ID for the currently authenticated user.
	 * @returns A promise that will be resolved to a M3 user ID.
	 */
	getUser(): Observable<string>;

	getUserContext(): Observable<IUserContext>;
}

/**
 * See [[IMIService]].
 * @internal
 */
class MIService implements IMIService {
	static baseUrl = "sh_rs"; // The default base URL for M3 in ION API
	static widgetContext: IWidgetContext2;
	static isIonApi = true;
	private userContextObservable: Observable<IUserContext>;
	private userObservable: Observable<string>;
	private miAccess: MIAccess;
	private userContext: IUserContext;
	private user: string;
	private pendingContext: AsyncSubject<IUserContext>[]; //ng.IDeferred<IUserContext>[];
	private httpClient: HttpClient;
	// tslint:disable-next-line:member-ordering
	//static $inject = ["$http", "$q", "$timeout"];

	// ge todo: figure out how to inject/instantiate http if it is still needed

	//constructor(private http: ng.IHttpService, public q: ng.IQService, public timeout: ng.ITimeoutService) {
	constructor() {
		const injector = Injector.create({
			providers: [
				{ provide: HttpClient, deps: [HttpHandler] },
				// tslint:disable-next-line:new-parens
				{ provide: HttpHandler, useValue: new HttpXhrBackend({ build: () => new XMLHttpRequest }) },
			],
		});


		this.httpClient = injector.get(HttpClient);
		this.miAccess = new MIAccess();
	}

	// static add(m: ng.IModule) {
	// 	m.service("m3MIService", MIService);
	// }
	getUser(): Observable<string> {
		//const deferred = this.q.defer();
		// const subject = new AsyncSubject<string>();
		if (this.userObservable) {
			return this.userObservable;
		}
		const subject = new AsyncSubject<string>();
		this.userObservable = subject.asObservable();
		this.getUserContext().subscribe((context: IUserContext) => {
			//deferred.resolve(context.USID);
			subject.next(context.USID);
			subject.complete();
		}, (r) => {
			//deferred.reject(r);
			subject.error(r);
		});
		//return deferred.promise as ng.IPromise<string>;
		return this.userObservable;
	}

	getUserContext(): Observable<IUserContext> {
		//const deferred = this.q.defer();
		if (this.userContextObservable) {
			return this.userContextObservable;
		}
		const subject = new AsyncSubject<IUserContext>();
		this.userContextObservable = subject.asObservable();
		//const subject = new AsyncSubject<IUserContext>();
		const userContext = this.userContext;
		if (userContext) {
			// The user context has already been loaded, resolve directly
			subject.next(userContext);
			subject.complete();
		} else {
			const pending = this.pendingContext;
			if (pending && pending.length > 0) {
				// A load request is already in progress, queue this deferred
				pending.push(subject);
			} else {
				// Start a new load request
				this.pendingContext = [subject];
				this.loadContext();
			}
		}
		return this.userContextObservable;
		//return subject.asObservable(); //deferred.promise;
	}

	execute(request: IMIRequest): Observable<IMIResponse> {
		const miAccess = this.miAccess;
		const baseUrl = "/" + miAccess.getDefaultBaseUrl();

		// tslint:disable-next-line:no-console

		const url = miAccess.createUrl(baseUrl, request);

		const subject = new AsyncSubject<IMIResponse>();
		//var deferred: ng.IDeferred<IMIResponse> = this.q.defer();

		const httpRequest = this.createRequest(url);

		// tslint:disable-next-line:no-unused-expression


		// httpRequest.url = "http://localhost:8083" + httpRequest.url;
		// this.executeHttp(httpRequest).subscribe(httpResponse => {
		// 	try {
		// 		const response = miAccess.parseResponse(request, httpResponse);
		// 		if (response.hasError()) {
		// 			subject.error(response);
		// 		} else {
		// 			subject.next(response);
		// 			subject.complete();
		// 		}
		// 	} catch (e) {
		// 		const errorResponse = new MIResponse();
		// 		errorResponse.error = e;
		// 		subject.error(errorResponse);
		// 	}
		// }, httpResponse => {

		// 	const response = new MIResponse();
		// 	const status = httpResponse.status;

		// 	const message = "Failed to call " + request.program + "." + request.transaction + " " + status;
		// 	response.errorMessage = message;

		// 	response.errorCode = (status) ? status.toString() : status;

		// 	subject.error(response);
		// });

		console.log("accessing url = " + "https://m3devapp.m3cedev.awsdev.infor.com" + httpRequest.url);
		this.httpClient.get("https://m3devapp.m3cedev.awsdev.infor.com" + httpRequest.url).subscribe(httpResponse => {
			try {
				const response = miAccess.parseResponse(request, httpResponse);
				if (response.hasError()) {
					subject.error(response);
				} else {
					subject.next(response);
					subject.complete();
				}
			} catch (e) {
				const errorResponse = new MIResponse();
				errorResponse.error = e;
				subject.error(errorResponse);
			}
		}, httpResponse => {

			const response = new MIResponse();
			const status = httpResponse.status;

			const message = "Failed to call " + request.program + "." + request.transaction + " " + status;
			response.errorMessage = message;

			response.errorCode = (status) ? status.toString() : status;

			subject.error(response);
		});


		// this.executeHttp(httpRequest).subscribe(httpResponse => {
		// 	try {
		// 		const response = miAccess.parseResponse(request, httpResponse.data);
		// 		// tslint:disable-next-line:no-console
		// 		console.log(response);
		// 		if (response.hasError()) {
		// 			subject.error(response);
		// 		} else {
		// 			subject.next(response);
		// 			subject.complete();
		// 		}
		// 	} catch (e) {
		// 		const errorResponse = new MIResponse();
		// 		errorResponse.error = e;
		// 		subject.error(errorResponse);
		// 	}
		// }, httpResponse => {
		// 	const response = new MIResponse();
		// 	const status = httpResponse.status;
		// 	// tslint:disable-next-line:no-console
		// 	console.log(response);
		// 	const message = "Failed to call " + request.program + "." + request.transaction + " " + status;
		// 	response.errorMessage = message;

		// 	response.errorCode = (status) ? status.toString() : status;

		// 	subject.error(response);
		// });

		return subject.asObservable();
	}

	private createRequest(url: string): IIonApiRequestOptions {
		return { method: "GET", url: url, responseType: "json", cache: false };
	}

	private executeHttp<T>(options: IIonApiRequestOptions): Observable<IIonApiResponse<T>> {
		//if (MIService.isIonApi) {
		return MIService.widgetContext.executeIonApiAsync(options);
		//}
		//return this.http(options);
	}

	private resolve<T>(items: AsyncSubject<T>[], value: any): void {
		for (const item of items) {
			item.next(value);
			item.complete();
		}
		// Clear the array
		items.splice(0, items.length);
	}

	private reject<T>(items: AsyncSubject<T>[], reason: any): void {
		for (const item of items) {
			item.error(reason);
		}
		// Clear the array
		items.splice(0, items.length);
	}

	private loadContext(): void {
		const pending = this.pendingContext;

		const request: IMIRequest = {
			program: "swagger.json",
			transaction: "programs",
			includeMetadata: false,
			outputFields: ["operationId", "description"]
		};

		this.execute(request).subscribe((response: IMIResponse) => {
			const userContext = response.item as IUserContext;
			this.userContext = userContext;
			// tslint:disable-next-line:no-console
			console.log(this.userContext);
			this.resolve(pending, userContext);
		}, (e) => {
			// Failed to load user context data
			this.reject(pending, e);
		});
	}

}

// TODO This class should only be used during development and not be included when the widgets are released if possible...
class MIServiceMock implements IMIService {
	//static $inject = ["$http", "$q", "$timeout"];
	// tslint:disable-next-line:ban-types
	static getMockData: Function;
	static user = "";
	static userContext: IUserContext;

	// tslint:disable-next-line:no-empty
	constructor() {
	}

	// static add(m: ng.IModule) {
	// 	m.service("m3MIService", MIService);
	// }

	getUser(): Observable<string> {
		//const deferred = this.q.defer();
		const subject = new AsyncSubject<string>();
		subject.next(MIServiceMock.user);
		subject.complete();
		return subject.asObservable();
	}

	getUserContext(): Observable<IUserContext> {
		//const deferred = this.q.defer();
		const subject = new AsyncSubject<IUserContext>();
		subject.next(MIServiceMock.userContext);
		subject.complete();
		return subject.asObservable();
	}

	execute(request: IMIRequest): Observable<IMIResponse> {
		//const deferred: ng.IDeferred<IMIResponse> = this.q.defer();
		const subject = new AsyncSubject<IMIResponse>();
		const response = new MIResponse();

		const getMockData = MIServiceMock.getMockData;
		if (getMockData) {
			const items = getMockData(request.program, request.transaction);
			response.items = items;
			if (items && items.length > 0) {
				response.item = items[0];
			}
		}

		subject.next(response);
		subject.complete();
		return subject.asObservable();
	}
}

/**
 * Represents a M3 bookmark.
 *
 * **Example**
 *
 * This example shows how to define a stateless bookmark definition for the E-panel.
 *
 *				private bookmark = <M3.Form.IBookmark> {
 *		program: "CRS610",
 *		table: "OCUSMA",
 *		keyNames: "OKCONO,OKCUNO",
 *		option: "5",
 *		panel: "E",
 *		isStateless: true
 *	};
 *
 * **Example**
 *
 * This example shows how to clone an existing bookmark definition and set some key values.
 * Note that CONO and DIVI fields can normally be left out since they are resolved automatically
 * using the M3 user context (if available).
 *
 *				var bookmark = angular.copy(this.bookmark);
 *	bookmark.values = {
 *		OKCONO: "100",
 *		OKCUNO: "TEST"
 *	};
 */
export interface IBookmark {
	[key: string]: any;
	/**
	 * Gets or sets the name of the bookmarked program.
	 */
	program?: string;

	/**
	 * Gets or sets the name of the database table.
	 */
	table?: string;

	/**
	 * Gets or sets the panel to start.
	 */
	panel?: string;

	/**
	 * Gets or sets the panel sequence.
	 * If the panel sequence is not set the Bookmark will start using the default panel sequence for the user.
	 */
	panelSequence?: string;

	/**
	 * Gets or sets the start panel. Use this property to override the default start panel for the program.
	 */
	startPanel?: string;

	/**
	 * Gets or sets a value that indicates if the start panel (A or B) should be included when the Bookmark is started.
	 */
	includeStartPanel?: boolean;

	/**
	 * Gets or sets the option to use when starting the Bookmark.
	 */
	option?: string;

	/**
	 * Gets or sets the sorting order to use when starting the Bookmark.
	 */
	sortingOrder?: string;

	/**
	 * Gets or sets the View to use when starting the Bookmark.
	 */
	view?: string;

	/**
	 * Gets or sets an optional name of a field that should get focus when a detail panel bookmark is started.
	 */
	focusFieldName?: string;

	/**
	 * Gets or sets a comma separated list of key names. Use this property or the keys property.
	 */
	keyNames?: string;

	/**
	 * Gets or sets a comma separated list of key names and values. Use this property or the keyNames property.
	 * The string should include all key names and values in the following pattern?: Key-1,Value-1,Key-2,Value-2,Key-N,Value-N.
	 * The values should be URL encoded using the UTF-8 encoding to ensure that that the keys string can be parsed correctly on the BE server.
	 */
	keys?: string;

	/**
	 * Gets or sets a comma separated list of parameter names and values.
	 */
	parameters?: string;

	/**
	 * Gets or sets a comma separated list of parameter names.
	 */
	parameterNames?: string;

	/**
	 * Gets or sets a comma separated list of field names and values. Used for for setting field values on start panels.
	 */
	fieldNames?: string;

	/**
	 * Gets or sets a comma separated list of field names and values. Used for for setting field values on start panels.
	 */
	fields?: string;

	/**
	 * Gets or sets a value that indicates if the bookmark should be statelenss or not.
	 * Stateless bookmarks will execute on the MUA server and then close the BE program before returning the response.
	 */
	isStateless?: boolean;

	/**
	 * Gets or sets an optional source application.
	 */
	source?: string;

	/**
	 * Gets or sets an automation XML string.
	 */
	automation?: string;

	/**
	 * Gets or sets the name of an automation template.
	 */
	automationTemplate?: string;

	/**
	 * Gets or sets a value that indicates if the bookmark must return an interactive panel or not.
	 * The default value is true. This property should only be set to false in special cases for bookmarks that do
	 * not return a panel. If this property is set to false no message will be displayed if the bookmark executed correctly
	 * but did not return a panel.
	 */
	requirePanel?: boolean;

	/**
	 * Gets or sets a value that indicates if initial confirm dialogs from BE should be automatically suppressed by pressing the ENTER key.
	 * The default value is false. Only use this property for special cases.
	 * There is currently no way to capture the message in the confirm dialog.
	 */
	suppressConfirm?: boolean;

	/**
	 *
	 */
	values?: any;

	/**
	 * Gets or sets the Information category used for customized lists.
	 */
	informationCategory?: string;

	/**
	 * Gets or sets the number of filters for customized lists.
	 */
	numberOfFilters?: string;

	/**
	 * Gets or sets additional request parameters that is not bookmark specific but will be included in the request.
	 */
	params?: any;
}

/**
 * @internal
 */
class BEConstants {
	static fieldInformationCategory = "WWIBCA";
	static fieldNumberOfFilters = "WWNFTR";
	static fieldHideCommandBar = "WWHICB";
}

/**
 * See [[IBookmark]]
 */
export class Bookmark {

	private static nameMap: { [id: string]: string } = {
		BM_PROGRAM: "program" as string,
		BM_TABLE_NAME: "tablename" as string,
		BM_PANEL: "panel" as string,
		BM_KEY_FIELDS: "keys" as string,
		BM_OPTION: "option" as string,
		BM_START_PANEL: "startpanel" as string,
		BM_FOCUS_FIELD_NAME: "focus" as string,
		BM_PANEL_SEQUENCE: "panelsequence" as string,
		BM_INCLUDE_START_PANEL: "includestartpanel" as string,
		BM_INQUIRY_TYPE: "sortingorder" as string,
		BM_VIEW: "view" as string,
		BM_SOURCE: "source" as string,
		BM_STATELESS: "stateless" as string,
		BM_START_PANEL_FIELDS: "fields" as string,
		BM_START_PANEL_FIELD_NAMES: "fieldnames" as string,
		BM_PARAMETERS: "parameters" as string,
		BM_AUTOMATION: "automation" as string,
		BM_AUTOMATION_TEMPLATE: "automationtemplate" as string,
		BM_SUPPRESS_CONFIRM: "suppressconfirm" as string,
		BM_REQUIRE_PANEL: "requirepanel" as string
	};

	private static getSource(bookmark: IBookmark) {
		return bookmark.source ? bookmark.source : "Web";
	}

	private static add(params: any, name: string, value: string) {
		if (name && value) {
			params[name] = value;
		}
	}

	private static addBool(params: any, name: string, value: boolean) {
		if (name) {
			params[name] = value ? "True" : "False";
		}
	}

	private static addValue(str: string, key: string, value: string): string {
		if (str.length > 0) {
			str += ",";
		}

		// Bookmark key values must be URL encoded in UTF-8 in the same way as done on the BE server.
		return str + key + "," + encodeURIComponent(value);
	}

	private static addInformationCategory(str: string, bookmark: IBookmark): string {
		str = Bookmark.addValue(str, BEConstants.fieldInformationCategory, bookmark.informationCategory);

		let filters = bookmark.numberOfFilters;
		if (!filters) {
			// Workaround for Foundation bug, always send 0 instead of blank value.
			filters = "0";
		}
		str = Bookmark.addValue(str, BEConstants.fieldNumberOfFilters, filters);
		return str;
	}

	private static createValues(userContext: IUserContext, keyString: string, values: any, isKeys: boolean, forceFallback?: boolean) {
		let str = "";

		const keys = keyString.split(",");
		// tslint:disable-next-line:prefer-for-of
		for (let i = 0; i < keys.length; i++) {
			const key = keys[i];
			let value = values[key];
			if (value === "" || (!value && isKeys) || forceFallback) {
				// Automatically convert the empty string to a blank that is expected by BE for optional values.
				// Always send blank space for missing keys, some key values are optional.
				// Fallbacks are used for key values.

				if (isKeys && key.length > 4) {
					// Fallback to the reference field name (ex FACI == WWFACI)
					const refKey = key.slice(2);
					const tempValue = values[refKey];
					if (tempValue && tempValue.length > 0) {
						// Fallback for CONO without prefix in values
						value = tempValue;
					} else {
						if (userContext) {
							// Fallback to the user context for CONO and DIVI
							if (refKey === "CONO") {
								value = userContext.CONO;
							} else if (refKey === "DIVI") {
								value = userContext.DIVI;
							}
						}
					}
					if (!value) {
						value = " ";
					}
				} else {
					value = " ";
				}
			}
			if (value) {
				str = Bookmark.addValue(str, key, value);
			}
		}
		return str;
	}

	// tslint:disable-next-line:member-ordering
	static toUriParams(bookmark: IBookmark, userContext?: IUserContext): string {
		const params = Bookmark.toParams(bookmark, userContext);
		let query = "";
		const nameMap = Bookmark.nameMap;
		// tslint:disable-next-line:forin
		for (const param in params) {
			const name = nameMap[param] || param;
			if (query) {
				query += "&";
			}
			query += name + "=" + encodeURIComponent(params[param]);
		}

		return query;
	}

	// tslint:disable-next-line:member-ordering
	static toParams(bookmark: IBookmark, userContext: IUserContext): any {
		const params = bookmark.params || {};

		this.add(params, "BM_PROGRAM", bookmark.program);
		this.add(params, "BM_PANEL_SEQUENCE", bookmark.panelSequence);
		this.add(params, "BM_START_PANEL", bookmark.startPanel);
		this.add(params, "BM_PANEL", bookmark.panel);
		this.add(params, "BM_FOCUS_FIELD_NAME", bookmark.focusFieldName);
		this.add(params, "BM_TABLE_NAME", bookmark.table);
		this.add(params, "BM_OPTION", bookmark.option);
		this.add(params, "BM_INQUIRY_TYPE", bookmark.sortingOrder);
		this.add(params, "BM_SOURCE", this.getSource(bookmark));
		this.add(params, "BM_VIEW", bookmark.view);
		this.add(params, "BM_AUTOMATION", bookmark.automation);
		this.add(params, "BM_AUTOMATION_TEMPLATE", bookmark.automationTemplate);

		this.addBool(params, "BM_INCLUDE_START_PANEL", bookmark.includeStartPanel);
		this.addBool(params, "BM_REQUIRE_PANEL", bookmark.requirePanel);
		this.addBool(params, "BM_SUPPRESS_CONFIRM", bookmark.suppressConfirm);

		if (bookmark.isStateless) {
			this.addBool(params, "BM_STATELESS", true);
		}

		const values = bookmark.values;

		let keys = bookmark.keys;
		if (bookmark.keyNames && values) {
			keys = Bookmark.createValues(userContext, bookmark.keyNames, values, true, true);
		}
		this.add(params, "BM_KEY_FIELDS", keys);

		let parameters = bookmark.parameters;
		if (bookmark.parameterNames && values) {
			parameters = Bookmark.createValues(userContext, bookmark.parameterNames, values, false);
		}
		this.add(params, "BM_PARAMETERS", parameters);

		let fields = bookmark.fields;
		const hasCategory = bookmark.informationCategory;
		if ((bookmark.fieldNames && values) || hasCategory) {
			fields = Bookmark.createValues(userContext, bookmark.fieldNames, values, false);
			if (hasCategory) {
				fields = Bookmark.addInformationCategory(fields, bookmark);
			}
		}
		this.add(params, "BM_START_PANEL_FIELDS", fields);

		return params;
	}

	// tslint:disable-next-line:member-ordering
	static urlToOptions(encodedUrl: string): IBookmark {
		const url = decodeURIComponent(encodedUrl);
		const urlLowerCase = url.toLowerCase();
		const nameMap = Bookmark.nameMap;
		const options = {} as IBookmark;

		const isSearchBookmark = url.indexOf("search?") !== -1;

		// Program follows bookmark? in regular bookmark, for search bookmark program will be found in nameMap and replaced correctly
		options.program = url.substr(url.indexOf("bookmark?") + 9, 6);

		for (const property in nameMap) {
			if (nameMap.hasOwnProperty(property)) {
				const index = urlLowerCase.indexOf(nameMap[property] + "=");
				if (index > 0 && (url[index - 1] === "&" || url[index - 1] === "?")) {
					const paramName = url.substr(index, nameMap[property].length);
					const valueStart = url.indexOf("=", index) + 1;
					const valueEnd = url.indexOf("&", index + 1);
					const paramValue = url.substring(valueStart, valueEnd === -1 ? url.length - 1 : valueEnd);

					if (paramValue) {
						this.addOptionFromValue(options, paramName, paramValue);
					}
				}
			}
		}

		if (isSearchBookmark) {
			// startpanelfields = fields
			let index = urlLowerCase.indexOf("startpanelfields=");
			if (index > 0 && (url[index - 1] === "&" || url[index - 1] === "?")) {
				const valueStart = url.indexOf("=", index) + 1;
				const valueEnd = url.indexOf("&", index + 1);
				const paramValue = decodeURIComponent(url.substring(valueStart, valueEnd === -1 ? url.length - 1 : valueEnd));

				if (paramValue) {
					this.addOptionFromValue(options, "fields", paramValue);
				}
			}

			// query = params.query
			index = urlLowerCase.indexOf("query=");
			if (index > 0 && (url[index - 1] === "&" || url[index - 1] === "?")) {
				const valueStart = url.indexOf("=", index) + 1;
				const valueEnd = url.indexOf("&", index + 1);
				const paramValue = url.substring(valueStart, valueEnd === -1 ? url.length - 1 : valueEnd);

				if (paramValue) {
					options.params = {
						query: decodeURIComponent(paramValue.replace(/\+/g, " "))
					};
				}
			}
		}

		options.table = options.tableName;
		return options;
	}

	private static addOptionFromValue(options: IBookmark, paramName: string, paramValue: string) {
		if (paramValue.indexOf(",") === -1) {
			options[paramName] = decodeURIComponent(paramValue.replace(/\+/g, " "));
		} else {
			let finalParamName: string;
			switch (paramName) {
				case "keys":
					finalParamName = "keyNames";
					break;
				case "fields":
					finalParamName = "fieldNames";
					break;
				case "parameters":
					finalParamName = "parameterNames";
					break;
				default:
					finalParamName = paramName;
					break;
			}

			options[finalParamName] = "";
			const valueArray = paramValue.split(",");
			for (let i = 0; i < valueArray.length; i++) {
				const value = decodeURIComponent(valueArray[i].replace(/\+/g, " "));
				if (i % 2 === 0) {
					options[finalParamName] += options[finalParamName] ? "," + value : value;
				} else {
					if (!options.values) {
						options.values = {};
					}
					options.values[valueArray[i - 1]] = value;
				}
			}
		}
	}
}

export class AngularCommon {
	private static isInitialized = false;
	private static miService: IMIService = null;
	private ht: HttpClient;

	static MIServiceInstance() {
		return AngularCommon.miService;
	}
	// tslint:disable-next-line:ban-types
	static initialize(widgetContext: IWidgetContext2, getMockData?: Function) {
		if (!AngularCommon.isInitialized) {
			AngularCommon.isInitialized = true;
			// TODO Temporary workaround for cloud vs on-premise paths
			// const baseUrl = context.isCloud() ? "M3" : "CustomerApi/M3";
			// MIService.baseUrl = "sh_rs";

			// Check for dev mode and overridden values.
			// if (widgetContext.isDev()) {
			// 	const application = widgetContext.getApplication() as any;
			// 	if (application) {
			// 		const isMock = application.m3DevServiceMock === "true";
			// 		if (isMock && getMockData) {
			// 			MIServiceMock.getMockData = getMockData;
			// 			MIServiceMock.user = application.m3User;
			// 			MIServiceMock.userContext = application.m3UserContext;
			// 			return;
			// 		}
			// 		const isIonApi = application.m3IonApi !== "false";
			// 		MIService.isIonApi = isIonApi;
			// 		//MIService.baseUrl = "http://" + application.hostname + ":" + application.port + (isIonApi ? "/M3" : "");
			// 		MIService.baseUrl = "http://" + application.hostname + ":" + application.port + (isIonApi ? "/M3" : "sh_rs");
			// 	} else {
			// 		MIService.isIonApi = true;
			// 		MIService.baseUrl = "";
			// 	}
			// }

			// tslint:disable-next-line:no-console
			// console.log(MIService.baseUrl);

			MIService.widgetContext = widgetContext;
			AngularCommon.miService = new MIService();
		}
	}

}
