interface IScreenList {
    [index: string]: {};
}
export const SCREENLIST = {} as IScreenList;
// tslint:disable-next-line:no-string-literal
SCREENLIST["BasketHead"] = [{
	screenId: "confirmation",
    params: [{"Id": "basketId"}]
}, {
    screenId: "customer-details",
    params: [{"Customer": "customerName"}, {"Id": "basketId"}]
}, {
    screenId: "customers-list",
    params: [{"Customer": "customerName"}, {"Id": "basketId"}]
}, {
	screenId: "delivery-addresses",
    params: [{"Customer": "customerName"}, {"Id": "basketId"}]
}, {
    screenId: "edit",
    params: [{"Id": "basketId"}]
}, {
    screenId: "new-order",
    params: [{"Customer": "customerName"}]
}, {
    screenId: "order-history",
    params: [{"Customer": "customerName"}, {"Id": "basketId"}]
}, {
	screenId: "payment",
    params: [{"Id": "basketId"}]
}, {
    screenId: "product",
    params: [{"Customer": "customerName"}, {"Id": "basketId"}]
}, {
    screenId: "shipping",
    params: [{"Id": "basketId"}]
}];

SCREENLIST["Customer"] = [{
    screenId: "customer-details",
    params: [{"Customer": "customerName"}, {"Id": "basketId"}]
}];
