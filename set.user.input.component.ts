import {
    CommonModule
} from "@angular/common";

import {
    FormsModule
} from "@angular/forms";

import {
    AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Injectable, Input,
    NgModule, OnDestroy, OnInit, Renderer2, ViewChild, ViewContainerRef, ViewEncapsulation
} from "@angular/core";

import {
    ExpandableAreaComponent, SohoModalDialogRef, SohoModalDialogService, SohoSearchFieldComponent,
    SohoToastService, SohoTreeComponent, SohoTreeService
} from "@infor/sohoxi-angular";

import {
    ArrayUtil, CommonUtil, DialogButtonType, DialogService, IDialogResult, ILanguage,
    ILaunchOptions, IMessageDialogOptions, IWidgetAction, IWidgetComponent, IWidgetContext2, IWidgetInstance2,
    Log, StandardDialogButtons, WidgetMessageType, WidgetState
} from "lime";

import {
    Bookmark, IBookmark, IMIRequest, IMIResponse, IMIService, IUserContext,
    AngularCommon, MIUtil
} from "./angular-common";

import {
    DataDisplayType, IInput, IM3MonitorDataset, IM3ViewerSettings, IMiBrowseDialogOptions
} from "./viewer.interfaces";

@Component({
    template: `
        <div *ngIf="input">
            <ng-container *ngFor="let item of input">
                <div *ngIf="item.userInput" class="field">
                    <label for="index-infor-m3-viewer">{{item.description}}</label>
                    <input id="index-infor-m3-viewer" [(ngModel)]="item.value" />
                </div>
            </ng-container>
        </div>
        <div class="modal-buttonset">
            <button class="btn-modal" (click)="onCancel()">{{translate("cancel")}}</button>
            <button class="btn-modal-primary" (click)="onOk()">{{translate("ok")}}</button>
        </div>
	`,
    styles: [`
	`],
    encapsulation: ViewEncapsulation.None
})

export class UserInputComponent implements OnInit {

    dialog: SohoModalDialogRef<UserInputComponent>;
    parameter: any;
    input: IInput[];

    private lang: ILanguage;

    ngOnInit() {
        this.input = JSON.parse(JSON.stringify(this.parameter.input)); // $.extend([], this.parameter.input);
        this.lang = this.parameter.lang;
    }

    onOk(): void {
        const result: IDialogResult = {
            button: DialogButtonType.Ok,
            value: this.input
        };
        this.dialog.close(result);
    }

    onCancel(): void {
        const result: IDialogResult = {
            button: DialogButtonType.Cancel
        };
        this.dialog.close(result);
    }

    translate(text: string): string {
        return this.lang ? this.lang.get(text) : "";
    }

}
