
import {
    CommonModule
} from "@angular/common";

import {
    FormsModule
} from "@angular/forms";

import {
    AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Injectable, Input,
    NgModule, OnDestroy, OnInit, Renderer2, ViewChild, ViewContainerRef, ViewEncapsulation
} from "@angular/core";

import {
    ExpandableAreaComponent, SohoBusyIndicatorDirective, SohoListViewComponent, SohoModalDialogRef,
    SohoModalDialogService, SohoSearchFieldComponent, SohoToastService, SohoTreeComponent, SohoTreeService
} from "@infor/sohoxi-angular";

import {
    ArrayUtil, CommonUtil, DialogButtonType, DialogService, IDialogResult,
    ILanguage, ILaunchOptions, IMessageDialogOptions, IWidgetAction, IWidgetComponent, IWidgetContext2,
    IWidgetInstance2, Log, StandardDialogButtons, WidgetMessageType,
    WidgetState
} from "lime";

import {
    Subject
} from "rxjs/Subject";

import {
    AsyncSubject
} from "rxjs/AsyncSubject";

import {
    Observable
} from "rxjs/Observable";

import {
    Bookmark, IBookmark, IMIRequest, IMIResponse, IMIService, IUserContext,
    AngularCommon, MIUtil
} from "./angular-common";

import {
    TransactionService
} from "./transaction.service";

interface IMiBrowseItem {
    name: string;
    desc: string;
    show: boolean;
}

import {
    Pipe,
    PipeTransform
} from "@angular/core";

import {
    SCREENLIST
} from "./screenid"

@Pipe({
    name: "searchFilter"
})

@Component({
    template: `
        <div *ngIf="lang">
            <div class="row">
                <div class="one-half column">
                    <div class="field">
                        <label for="infor-m3-viewer-search-name">{{translate("name")}}</label>
                        <div class="searchfield-wrapper lm-margin-md-b">
                            <input id="infor-m3-viewer-search-name" [(ngModel)]="queryName" (ngModelChange)="onSearch()" class="searchfield" (keyup.enter)="onEnter()" />
                            <svg class="icon" focusable="false">
                                <use xlink:href="#icon-search"></use>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="one-half column">
                    <div class="field">
                        <label for="infor-m3-viewer-search-descr">{{translate("description")}}</label>
                        <div class="searchfield-wrapper lm-margin-md-b">
                            <input id="infor-m3-viewer-search-descr" [(ngModel)]="queryDesc" (ngModelChange)="onSearch()" class="searchfield" (keyup.enter)="onEnter()" />
                            <svg class="icon" focusable="false">
                                <use xlink:href="#icon-search"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="lm-margin-zero-l lm-margin-md-b" />
            <div soho-busyindicator [activated]="isBusy">
                <soho-listview class="saleshub-browser-list" [ngStyle] = "{height: '200px'}"
                    (selected)="onSelected($event)"
                    #singleSelectListView>
                        <li soho-listview-item class="" *ngFor="let item of filteredData" [selected]="item.selected" >
                            <div class="one-half column">
                                <p>{{item.name}}</p>
                            </div>
                            <div class="one-half column">
                                <p>{{item.desc}}</p>
                            </div>

                        </li>
                </soho-listview>
            </div>
        </div>
        <div class="modal-buttonset">
            <button class="btn-modal" (click)="onClose()">{{translate("cancel")}}</button>
            <button class="btn-modal-primary" (click)="onSelect()">{{translate("select")}}</button>
        </div>

	`,

    styles: [`
    .saleshub-browser-list li .column:nth-child(2) p {
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
    }

    .saleshub-browser-list li .column:nth-child(2) {
        margin-left: 40px;
    }

    .saleshub-browser-list li .column:first-child {
        margin-left: 0;
    }
	`],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})

export class BrowseDialogComponent implements AfterViewInit {
    @Input() widgetContext: IWidgetContext2;
    @Input() widgetInstance: IWidgetInstance2;

    // @ViewChild("widgetView", { read: ViewContainerRef }) widgetView: ViewContainerRef;
    @ViewChild(SohoListViewComponent) listView: SohoListViewComponent;
    @ViewChild("singleSelectListView") listViewElement: any;

    //    @Input() lang: ILanguage;
    dialog: SohoModalDialogRef<BrowseDialogComponent>;
    parameter: any;
    lang: ILanguage;

    private listViewData: IMiBrowseItem[] = [];
    private filteredData: IMiBrowseItem[] = [];
    private nrToList: number = 25;
    private selectedItem: IMiBrowseItem;
    private isBusy: boolean;
    private processingBottom: boolean;
    private queryName: string;
    private queryDesc: string;

    constructor(private changeDetection: ChangeDetectorRef, private dialogService: DialogService, private sohoDialogService: SohoModalDialogService) {

    }

    ngOnInit() {

        this.lang = this.parameter.lang;
        this.widgetContext = this.parameter.widgetContext;
    }

    ngAfterViewInit() {
        this.load();
    }

    translate(text: string): string {
        return this.lang ? this.lang.get(text) : "";
    }
    onClose(): void {
        this.dialog.close();
        this.dialog = null;
    }

    onSelect(): void {
        if (this.selectedItem) {
            const result: IDialogResult = {
                value: this.selectedItem.name
            };
            this.dialog.close(result);
            this.dialog = null;
        }
    }

    private load() {
        this.loadData();
        this.scrollEvent();
    }

    private onSelected(event: any) {
        const selectedIndex = this.listViewElement.getSelectedItems;
        if (selectedIndex && selectedIndex.length) {
            this.selectedItem = this.filteredData[selectedIndex[0]];
        } else {
            this.selectedItem = undefined;
        }
    }

    private onSearch() {
        this.filter(this.listViewData, this.queryName, this.queryDesc, this.nrToList);
    }

    private scrollEvent() {
        $(this.listViewElement.jQueryElement).bind("scroll", (e: any) => {
            if (Math.round($(e.currentTarget)[0].scrollHeight - $(e.currentTarget).scrollTop()) === Math.round($(e.currentTarget).outerHeight())) {
                if (this.listViewData.length > this.nrToList) {
                    if ((this.nrToList + 25) > this.listViewData.length) {
                        //Present all items left, >25
                        this.nrToList = this.nrToList + (this.listViewData.length - this.nrToList);
                    } else {
                        //Present 25 more
                        this.nrToList += 25;
                    }
                    this.filter(this.listViewData, this.queryName, this.queryDesc, this.nrToList);
                }
            }
        });

    }

    private setupKeyPressListener(): void {
        const self = this;
        $("#infor-m3-viewer-search-name, #infor-m3-viewer-search-descr").keypress((e: any) => {
            if (e.which === 13) {
                if (self.filteredData && self.filteredData.length === 1) {
                    self.selectedItem = self.filteredData[0];
                    self.onSelect();
                }
            }
        });
    }

    private onEnter() {
        if (this.filteredData && this.filteredData.length === 1) {
            this.selectedItem = this.filteredData[0];
            this.onSelect();
        }
    }
    private loadData() {
        const browseOptions = this.parameter.browseOptions;
        const selectedService = this.parameter.selectedService;
        if (browseOptions) {
            this.isBusy = true;
            this.changeDetection.detectChanges();
            this.parameter.cache = false;

            TransactionService.listMiData(browseOptions, this.parameter.cache).subscribe((items: any) => {
                for (const item of items) {
                    const listItem: IMiBrowseItem = {
                        name: item[browseOptions.outputFields[0]],
                        desc: item[browseOptions.outputFields[1]],
                        show: true
                    };
                    if (!ArrayUtil.containsByProperty(this.listViewData, "name", listItem.name)) {
                        this.listViewData.push(listItem);
                    }
                }
                this.filter(this.listViewData, this.queryName, this.queryDesc, this.nrToList);
                this.isBusy = false;
                this.changeDetection.detectChanges();
            }, (errorResponse) => {
                this.isBusy = false;
                this.changeDetection.detectChanges();
                this.dialogService.showMessage({ title: this.translate("requestFailed"), message: errorResponse.errorMessage, isError: true });
            });
        } else if (selectedService) {
            this.loadPages(selectedService);
        }
    }

    private loadPages(selectedService: string) {
        this.isBusy = true;
        this.changeDetection.detectChanges();
        this.parameter.cache = false;

        let serviceToPages:any = {};
        serviceToPages = SCREENLIST;
        const pagesArray = serviceToPages[selectedService];

        if (pagesArray === undefined || pagesArray.length === 0) {
            const listItem: IMiBrowseItem = {
                name: "No page available for " + selectedService,
                desc: "No page available for " + selectedService,
                show: true
            };
            if (!ArrayUtil.containsByProperty(this.listViewData, "name", listItem.name)) {
                this.listViewData.push(listItem);
            }
        } else {
            for (const item of pagesArray) {
                const listItem: IMiBrowseItem = {
                    name: item["screenId"],
                    desc: item["screenId"],
                    show: true
                };
                if (!ArrayUtil.containsByProperty(this.listViewData, "name", listItem.name)) {
                    this.listViewData.push(listItem);
                }
            }
        }
        this.filter(this.listViewData, this.queryName, this.queryDesc, this.nrToList);
        this.isBusy = false;
        this.changeDetection.detectChanges();
    }

    private filter(value: any[], name: string, desc: string, limit: number) {
        let itemsCount = 0;
        //if (!term) return value;
        this.filteredData = (value || []).filter((item: any, index: number, array: any[]) => {
            if (itemsCount >= limit) {
                return false;
            }
            const retVal = RegExp(name, "gi").test(item.name) && RegExp(desc, "gi").test(item.desc);
            if (retVal) {
                itemsCount++;
            }
            return retVal;
        });
        this.changeDetection.detectChanges();

    }

    private selected(item: IMiBrowseItem) {
        this.selectedItem = item;
    }

}
