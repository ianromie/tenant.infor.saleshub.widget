﻿import "lime";
import { IUserContext, MIUtil } from "./angular-common";
import { IInput } from "./viewer.interfaces";

export class M3CommonUtil {
	/**
	 * Get specified input as a record.
	 * @param input
	 */
	static getInputAsRecord(inputFields: IInput[], userContext: IUserContext, drilldownItem?: any): Object {
		const inputObject: any = {};
		let val: string;
		for (const input of inputFields) {
			// Resolve any user context or date macros, or if not found will be a hardcoded input
			val = this.resolveAndReplace(input.value, userContext, drilldownItem);
			inputObject[input.field] = val;

		}

		return inputObject;
	}

	private static resolveAndReplace(input: string, userContext: IUserContext, drilldownItem?: any): string {
		let result = input;
		let done: boolean;
		let macroStart: number;
		let macroEnd: number;

		// Only perform if drilldown mode & input contains chart value mapping macro
		if (drilldownItem && input.indexOf("{@") !== -1 && input.indexOf("}") !== - 1) {
			do {
				// Chart value mapping macro
				done = true;
				macroStart = result.indexOf("{@", macroStart || 0);
				if (macroStart !== -1) {
					done = false;
					macroEnd = result.indexOf("}", macroStart);
					if (macroEnd !== -1) {
						const expr = result.substr(macroEnd - 4, 4);
						const chartMapVal = drilldownItem.data ? drilldownItem.data[expr] : drilldownItem.drilldownData[expr];
						if (chartMapVal) {
							const toReplace = result.substring(macroStart, macroEnd + 1);
							result = result.replace(toReplace, chartMapVal);
						} else {
							macroStart++;
						}
					} else {
						done = true;
					}
				}
			} while (!done);
		}

		// Only perform if input contains user context macro
		if (input.indexOf("{") !== -1 && input.indexOf("}") !== -1) {
			do {
				// User context macro
				done = true;
				macroStart = result.indexOf("{", macroStart || 0);
				if (macroStart !== -1) {
					done = false;
					macroEnd = result.indexOf("}", macroStart);
					if (macroEnd !== -1) {
						const expr = result.substring(macroStart + 1, macroEnd);
						const contextVal = (userContext as any)[expr];
						if (contextVal) {
							const toReplace = result.substring(macroStart, macroEnd + 1);
							result = result.replace(toReplace, contextVal);
						} else {
							macroStart++;
						}
					} else {
						done = true;
					}
				}
			} while (!done);
		}

		// Only perform if input contains date macro
		if (input.indexOf("DATE(") !== -1) {
			do {
				// Date macro
				done = true;
				macroStart = result.indexOf("DATE(", macroStart || 0);
				if (macroStart !== -1) {
					done = false;
					macroEnd = result.indexOf(")", macroStart);
					if (macroEnd !== -1) {
						const dayAddition = parseInt(result.substring(macroStart + 5, macroEnd));
						const today = new Date();
						let dateString: string;
						if (dayAddition) {
							const dateResult = new Date(today.setDate(today.getDate() + dayAddition));
							dateString = MIUtil.getDateFormatted(dateResult);
						} else {
							dateString = MIUtil.getDateFormatted(today);
						}
						if (dateString) {
							let userFormattedDateString: string;
							// Adher to user date format in MNS150
							switch (userContext.DTFM) {
								case "DMY":
									userFormattedDateString = dateString.substr(6, 2) + dateString.substr(4, 2) + dateString.substr(0, 4);
									break;
								case "MDY":
									userFormattedDateString = dateString.substr(4, 2) + dateString.substr(6, 2) + dateString.substr(0, 4);
									break;
								case "YMD":
								default:
									userFormattedDateString = dateString;
									break;
							}
							const toReplace = result.substring(macroStart, macroEnd + 1);
							result = result.replace(toReplace, userFormattedDateString);
						} else {
							macroStart++;
						}
					} else {
						done = true;
					}
				}
			} while (!done);
		}

		return result;
	}
}
