import {
    CommonModule
} from "@angular/common";

import {
    FormsModule
} from "@angular/forms";

import {
    AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Injectable, Input,
    NgModule, OnChanges, OnDestroy, Output, Renderer2, SimpleChanges,
    ViewChild, ViewContainerRef, ViewEncapsulation
} from "@angular/core";

import {
    ExpandableAreaComponent, SohoModalDialogRef, SohoModalDialogService, SohoSearchFieldComponent,
    SohoToastService, SohoTreeComponent, SohoTreeService
} from "@infor/sohoxi-angular";

import {
    ArrayUtil, CommonUtil, DialogButtonType, DialogService, IDialogResult, ILanguage,
    ILaunchOptions, IMessageDialogOptions, IWidgetAction, IWidgetComponent, IWidgetContext2, IWidgetInstance2,
    Log, StandardDialogButtons, WidgetMessageType, WidgetState
} from "lime";

import {
    Subject
} from "rxjs/Subject";

import {
    AsyncSubject
} from "rxjs/AsyncSubject";

import {
    Observable
} from "rxjs/Observable";

import {
    Bookmark, IBookmark, IMIRequest, IMIResponse, IMIService, IUserContext,
    AngularCommon, MIUtil
} from "./angular-common";

import {
    DataDisplayType, IM3MonitorDataset, IM3ViewerSettings, IMiBrowseDialogOptions
} from "./viewer.interfaces";

import {
    BrowseDialogComponent
} from "./browser.dialog.component";

import {
    InputOutputComponent
} from "./input.output.component";

interface IDisplayOption {
    value: DataDisplayType;
    label: string;
}

@Component({
    selector: "m3-viewer-transaction",
    template: `
    <div #transactionDialog class="container" >
        <div *ngIf="settings">
            <div>
                <label for="infor-m3-viewer-settings-program">{{translate("service")}}</label>
                <span class="lookup-wrapper">
                    <!--<input id="infor-m3-viewer-settings-program" readonly="readonly" class="viewer-settings-padding" [(ngModel)]="settings.program" class="lookup"  />-->
                    <input id="infor-m3-viewer-settings-program" readonly="readonly" class="lookup" [(ngModel)]="settings.program"   />
                    <button class="inputButton"
                        soho-button="icon"
                        [icon]="'search-list'"
                        (click)="selectProgram()">
                    </button>
                </span>
            </div>
            <div>
                <label for="infor-m3-viewer-settings-transaction">{{translate("operation")}}</label>
                <span class="lookup-wrapper">
                    <input id="infor-m3-viewer-settings-transaction" readonly="readonly" [(ngModel)]="settings.transaction" class="lookup"  />
                    <button class="inputButton"
                        soho-button="icon"
                        [icon]="'search-list'"
                        (click)="selectTransaction()">
                    </button>
                </span>
            </div>
            <div>
                <label for="infor-m3-viewer-settings-input">{{translate("input")}}</label>
                <span class="lookup-wrapper">
                    <input id="infor-m3-viewer-settings-input" readonly="readonly" [(ngModel)]="displayInput" class="lookup"  />
                    <!--<span class="trigger" tabindex="-1" (click)="setInputOutput('input')">
                        <svg soho-icon [icon]="'search-list'"></svg>
                    </span>-->
                    <button class="inputButton"
                        soho-button="icon"
                        [icon]="'search-list'"
                        (click)="setInputOutput('input')">
                    </button>
                </span>
            </div>
            <div *ngIf="mode !== 'monitor' || settings.enableDrilldown === true">
                <label for="infor-m3-viewer-settings-output">{{translate("output")}}</label>
                <span class="lookup-wrapper">
                    <input id="infor-m3-viewer-settings-output" readonly="readonly" [(ngModel)]="displayOutput" class="lookup"  />
                    <!--<span class="trigger" tabindex="-1" (click)="setInputOutput('output')">
                        <svg soho-icon [icon]="'search-list'"></svg>
                    </span>-->

                    <button class="inputButton"
                        soho-button="icon"
                        [icon]="'search-list'"
                        (click)="setInputOutput('output')">
                    </button>
                </span>
            </div>
            <div *ngIf="mode !== 'monitor' || settings.enableDrilldown === true">
                <label for="infor-m3-viewer-settings-page">{{translate("page")}}</label>
                <span class="lookup-wrapper">
                    <input id="infor-m3-viewer-settings-page" readonly="readonly" [(ngModel)]="displayPage" class="lookup"  />
                    <button class="inputButton"
                        soho-button="icon"
                        [icon]="'search-list'"
                        (click)="setPage('page')">
                    </button>
                </span>
            </div>
            <div class="row" *ngIf="mode === 'viewer-settings' || settings.enableDrilldown === true">
                <div class="field">
                    <label for="infor-m3-viewer-settings-displayAs">{{translate("displayAs")}}</label>
                    <select soho-dropdown name="infor-m3-viewer-settings-displayAs" [(ngModel)]="settings.displayType"  style="width: 106px">
                        <option *ngFor="let option of displayOptions" [ngValue]="option.value">{{option.label}}</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
	`,
    styles: [`
    m3-viewer-transaction .lookup-wrapper button.inputButton {
        position: absolute;
        top: 0px;
        right: 1px
    }
	`],
    encapsulation: ViewEncapsulation.None
})

export class TransactionComponent implements AfterViewInit, OnDestroy, IWidgetComponent, OnChanges {
    @Input() widgetContext: IWidgetContext2;
    @Input() widgetInstance: IWidgetInstance2;
    @Input() miService: IMIService;
    @Input() settings: IM3MonitorDataset;
    @Input() mode: string;

    @ViewChild("transactionDialog", { read: ViewContainerRef }) transactionDialog: ViewContainerRef;

    @Output() inputCallback = new EventEmitter();
    private lang: ILanguage;
    private displayInput: string;
    private displayOutput: string;
    private displayPage: string;
    private displayOptions: IDisplayOption[];
    private logPrefix = "[M3Environment] ";
    // private miService: IMIService;

    constructor(private changeDetection: ChangeDetectorRef, private sohoDialogService: SohoModalDialogService) {

    }

    save() {
        this.updateInputOutputViews();
        this.updatePage();
    }

    ngOnChanges(changes: SimpleChanges) {
        const settingChanges = changes.settings;
        if (settingChanges) {
            this.updateInputOutputViews();
            this.updatePage();
        }
    }

    ngOnInit() {
        this.miService = AngularCommon.MIServiceInstance();
        this.lang = this.widgetContext.getLanguage();

        this.displayOptions = [
            { value: DataDisplayType.List, label: this.translate("listView") },
            { value: DataDisplayType.Card, label: this.translate("cardView") }
        ];
    }

    ngAfterViewInit() {
        try {
            //this.changeDetection.detectChanges();
        } catch (error) {
            this.showErrorMessage("", error);
        }
    }

    ngOnDestroy() {
        // if (this.refreshTimeoutId) {
        //     window.clearTimeout(this.refreshTimeoutId);
        // }
    }

    private showErrorMessage(displayMsg: string, logMsg: string): void {
        Log.debug(this.logPrefix + logMsg);
        this.widgetContext.showWidgetMessage({ type: WidgetMessageType.Error, message: displayMsg });
    }

    private setBusy(isBusy: boolean): void {
        this.widgetContext.setState(isBusy ? WidgetState.busy : WidgetState.running);
    }

    private showMessage(displayMsg: string): void {
        this.widgetContext.showWidgetMessage({ type: WidgetMessageType.Info, message: displayMsg });
    }

    private translate(text: string): string {
        return this.lang ? this.lang.get(text) : "";
    }

    private selectProgram(): void {
        const dialog = this.sohoDialogService
            .modal(BrowseDialogComponent, this.transactionDialog)
            .title(this.translate("selectService"))
            .afterClose((result: IDialogResult) => {
                if (result && result.value && result.value !== this.settings.program) {
                    this.settings.program = result.value;
                    this.settings.transaction = "";

                    this.clearDependantSettings();
                    //this.changeDetection.detectChanges();
                }
            });

        dialog.apply((component) => {
            const browseOptions: IMIRequest = {
                program: "swagger.json",
                transaction: "programs",
                maxReturnedRecords: 0,
                outputFields: ["operationId", "description"]
            };
            component.parameter = { lang: this.lang, browseOptions: browseOptions, widgetContext: this.widgetContext };
            component.dialog = dialog;
        }).open();

    }

    private selectTransaction(): void {
        //const settings = this.settings;
        if (this.settings.program) {
            const browseOptions: IMIRequest = {
                // program: "MRS001MI",
                // transaction: "LstTransactions",
                // maxReturnedRecords: 0,
                // outputFields: ["TRNM", "TRDS"],
                // record: {
                //     MINM: this.settings.program
                // }

                program: "swagger.json",
                transaction: "transactions",
                maxReturnedRecords: 0,
                outputFields: ["operationId", "description"],
                record: {
                    MINM: this.settings.program
                }
            };
            const dialog = this.sohoDialogService
                .modal(BrowseDialogComponent, this.transactionDialog)
                .title(this.translate("selectOperation"));

            dialog.apply((component) => {
                //                component.lang = this.lang;
                component.parameter = { lang: this.lang, browseOptions: browseOptions };
                component.dialog = dialog;
            }).afterClose((result: IDialogResult) => {
                if (result && result.value && result.value !== this.settings.transaction) {
                    this.settings.transaction = result.value;
                    this.clearDependantSettings();
                    //this.changeDetection.detectChanges();
                }
            }).open();
        }
    }

    private setInputOutput(type: string): void {

        //const settings = this.settings;
        if (this.settings.program && this.settings.transaction) {
            // tslint:disable-next-line:no-console

            const dialog = this.sohoDialogService
                .modal(InputOutputComponent, this.transactionDialog)
                .title(this.translate("ioParams"));

            dialog.apply((component) => {

                component.lang = this.lang;
                component.parameter = { lang: this.lang, type: type, settings: this.settings };
                component.dialog = dialog;
                component.ioSettings = this.settings ? $.extend({ refreshRate: undefined }, this.settings) as IM3ViewerSettings : undefined;
            }).afterClose((result: IDialogResult) => {
                if (result && result.value) {
                    this.settings.input = result.value.input;
                    if (this.inputCallback) {
                        this.inputCallback.next();
                    }
                    if (JSON.stringify(this.settings.output) !== JSON.stringify(result.value.output)) {
                        this.settings.output = result.value.output;
                        if (this.settings.chartSettings && (this.mode === "viewer-settings" || this.settings.enableDrilldown)) {
                            this.settings.chartSettings.labelColumn = "";
                        }
                    }
                    if (this.mode === "viewer-settings" || this.settings.enableDrilldown || this.mode === "drilldown") {
                        this.settings.maxRecords = result.value.maxRecords;
                        this.settings.enableBookmarks = result.value.enableBookmarks;
                        this.settings.bookmarkData = result.value.bookmarkData;
                        this.settings.bookmarkType = result.value.bookmarkType;
                    }
                    this.updateInputOutputViews();
                    //this.changeDetection.detectChanges();
                }
            }).afterOpen((r: any) => {
                //console.log("afterOpen", r);

            }).open();
        }
    }

    private setPage(): void {
        if (this.settings.program) {
            const dialog = this.sohoDialogService
            .modal(BrowseDialogComponent, this.transactionDialog)
            .title(this.translate("selectPage"));
            dialog.apply((component) => {
                component.parameter = { lang: this.lang, selectedService: this.settings.program };
                component.dialog = dialog;
            }).afterClose((result: IDialogResult) => {
                if (result && result.value && result.value !== this.settings.page) {
                    this.settings.page = result.value;
                    this.updatePage();
                }
            }).open();
        }
    }

    private updatePage(): void {
        this.displayPage = this.settings.page;
    }

    private updateInputOutputViews(): void {
        let inputText = "";
        let outputText = "";
        const settings = this.settings;

        if (!settings.input) {
            settings.input = [];
        }
        for (const setting of settings.input) {
            //const setting = settings.input[i];
            inputText += setting.field;
            if (setting.value.length) {
                inputText += "=" + setting.value;
            }
            inputText += ";";
        }
        if (!settings.output) {
            settings.output = [];
        }
        for (const setting of settings.output) {
            outputText += setting.field + ";";
        }

        this.displayInput = inputText;
        this.displayOutput = outputText;
        //this.changeDetection.detectChanges();
    }

    private clearDependantSettings(): void {
        //const settings = this.settings;
        this.settings.input = [];
        this.settings.output = [];
        this.settings.page = "";
        this.displayInput = "";
        this.displayOutput = "";
        this.displayPage = "";
        if (this.mode === "viewer-settings" || this.settings.enableDrilldown || this.mode === "drilldown") {
            if (this.settings.chartSettings) {
                this.settings.chartSettings.groupByLabel = false;
                this.settings.chartSettings.valueColumns = [];
                this.settings.chartSettings.labelColumn = "";
            }
            this.settings.bookmarkData = {};
            this.settings.enableBookmarks = false;
        }
    }
}
