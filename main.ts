﻿import {
	CommonModule
} from "@angular/common";

import {
	FormsModule
} from "@angular/forms";

import {
	AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Injectable, Input, NgModule,
	OnDestroy, OnInit, Renderer2, ViewChild, ViewEncapsulation
} from "@angular/core";

import {
	ExpandableAreaComponent, SohoBusyIndicatorModule, SohoButtonModule, SohoChartModule, SohoContextMenuModule,
	SohoDataGridModule, SohoDropDownModule, SohoExpandableAreaModule, SohoIconModule, SohoInputModule, SohoLabelModule,
	SohoListViewModule,
	SohoModalDialogModule, SohoPieModule, SohoPopupMenuModule, SohoSearchFieldModule, SohoTabsModule,
	SohoToastService
	// SohoComponentsModule,
} from "@infor/sohoxi-angular";

import {
	ArrayUtil, CommonUtil, DialogButtonType, DialogService, IApplication,
	ILanguage, ILaunchOptions, IWidgetAction, IWidgetComponent, IWidgetContext2, IWidgetInstance2,
	IWidgetSettingsContext2, IWidgetSettingsInstance2, Log, StandardDialogButtons, WidgetMessageType, WidgetState
} from "lime";

import {
	Subject
} from "rxjs/Subject";

import {
	AsyncSubject
} from "rxjs/AsyncSubject";

import {
	Observable
} from "rxjs/Observable";

import {
	Bookmark, IBookmark, IMIRequest, IMIResponse, IMIService, IUserContext,
	AngularCommon, MIRecord, MIUtil
} from "./angular-common";

import {
	TitleSettingComponent, ViewerSettingsComponent
} from "./viewer.settings.component";

// import {
// 	Test
// } from "m3.common.viewer"

import {
	ViewerModule
} from "./viewer.component";

@Component({
	template: `
		<m3-viewer *ngIf="widgetContext && widgetInstance && miService" [widgetContext]="widgetContext" [widgetInstance]="widgetInstance" [miService]="miService"></m3-viewer>
	`,
	styles: [`
	`],
	//changeDetection: ChangeDetectionStrategy.OnPush,
	encapsulation: ViewEncapsulation.None
})
export class SalesHubViewerContainerComponent implements OnInit, IWidgetComponent {
	@Input() widgetContext: IWidgetContext2;
	@Input() widgetInstance: IWidgetInstance2;
	miService: IMIService;

	private logPrefix = "[M3 Viewer Widget] ";
	private orderBy: string;
	private refreshTimeoutId: number;

	private lang: ILanguage;
	private userContext: IUserContext;
	constructor(private changeDetection: ChangeDetectorRef, private dialogService: DialogService) {

	}

	ngOnInit() {
		try {
			this.miService = AngularCommon.MIServiceInstance();
			this.lang = this.widgetContext.getLanguage();

			this.widgetInstance.widgetSettingsFactory = (context: IWidgetSettingsContext2): IWidgetSettingsInstance2 => {
				return {
					angularConfig: {
						componentType: ViewerSettingsComponent,
					},
				};
			};

			//this.changeDetection.detectChanges();
		} catch (error) {
			Log.error(error);
		}
	}
}

@NgModule({
	imports: [CommonModule, FormsModule, ViewerModule, SohoBusyIndicatorModule, SohoButtonModule, SohoChartModule, SohoContextMenuModule,
		SohoDataGridModule, SohoDropDownModule, SohoExpandableAreaModule, SohoIconModule, SohoInputModule, SohoLabelModule,
		SohoListViewModule,
		SohoModalDialogModule, SohoPieModule, SohoPopupMenuModule, SohoSearchFieldModule, SohoTabsModule],
	declarations: [SalesHubViewerContainerComponent, ViewerSettingsComponent, TitleSettingComponent],
	providers: [],
	entryComponents: [SalesHubViewerContainerComponent, ViewerSettingsComponent, TitleSettingComponent]
})
export class SalesHubViewerModule {
}
