
import {
	CommonModule
} from "@angular/common";

import {
	FormsModule
} from "@angular/forms";

import {
	AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Injectable, Input,
	NgModule, OnDestroy, Renderer2, ViewChild, ViewContainerRef, ViewEncapsulation
} from "@angular/core";

import {
	ExpandableAreaComponent, SohoDataGridComponent, SohoModalDialogRef, SohoModalDialogService,
	SohoSearchFieldComponent, SohoToastService, SohoTreeComponent, SohoTreeService
} from "@infor/sohoxi-angular";

import {
	ArrayUtil, CommonUtil, DialogButtonType, DialogService, IDialogResult,
	ILanguage, ILaunchOptions, IMessageDialogOptions, IWidgetAction, IWidgetComponent, IWidgetContext2,
	IWidgetInstance2, Log, StandardDialogButtons, WidgetMessageType,
	WidgetState
} from "lime";

import {
	Bookmark, IBookmark, IMIRequest, IMIResponse, IMIService, IUserContext,
	AngularCommon, MIUtil
} from "./angular-common";

import {
	TransactionService
} from "./transaction.service";

import {
	BookmarkSupport, BookmarkType, IM3ViewerSettings, IMiBrowseDialogOptions
} from "./viewer.interfaces";

@Component({
	template: `
		<div id="inputOutputView" soho-busyindicator blockUI="true" [activated]="isBusy" *ngIf="ioSettings" [ngStyle]="{'height':'500px'}">
			<!--(activated)="onTabActivated($event)"-->
			<div soho-tabs  [tabsOptions]="{ 'appMenuTrigger': true }">
				<div soho-tab-list-container>
					<ul soho-tab-list>
						<li soho-tab><a soho-tab-title tabId="infor-m3-viewer-input-tab">{{translate("input")}}</a></li>
						<li soho-tab [selected]="tab === 'output'"><a soho-tab-title tabId="infor-m3-viewer-output-tab">{{translate("output")}}</a></li>
					</ul>
				</div>
			</div>
			<div soho-tab-panel-container style="height: calc(100% - 40px); margin-bottom: 0px;">
				<div soho-tab-panel tabId="infor-m3-viewer-input-tab" style="height:100%">
					<div>
						<div #inforM3ViewerInputDatagrid soho-datagrid class="lm-width-full m3-info-viewer-datagrid datagrid-container" [hidden]="!inputDatagridOptions">
						</div>
						<p *ngIf="noInputFieldErrorMsg">
							<br/>
							{{noInputFieldErrorMsg}}
						</p>
					</div>
				</div>

				<div soho-tab-panel tabId="infor-m3-viewer-output-tab" style="height:100%">
					<div>
						<div #inforM3ViewerOutputDatagrid soho-datagrid class="lm-width-full m3-info-viewer-datagrid datagrid-container" [hidden]="!outputDatagridOptions">

						</div>
						<div>
							<div class="field lm-margin-xl-t">
								<label for="infor-m3-viewer-max-records">{{translate("maxRecords")}}</label>
								<select soho-dropdown name="infor-m3-viewer-max-records" [(ngModel)]="ioSettings.maxRecords">
									<option *ngFor="let option of recordCounts" [ngValue]="option">{{option}}</option>
								</select>

							</div>
							<div *ngIf="isBookmarkSupported">
								<div class="field lm-margin-md-b">
									<div class="switch lm-margin-zero-t">
										<input type="checkbox" class="switch" id="infor-m3-viewer-enable-bookmarks" (ngModelChange)="onEnableBookmarksChange()" [(ngModel)]="ioSettings.enableBookmarks">
										<label for="infor-m3-viewer-enable-bookmarks">{{translate("enableBookmarks")}}</label>
									</div>
								</div>
								<div class="row">
									<div class="one-third column">
										<div class="field">
											<label for="infor-m3-viewer-bookmark-option">{{translate("option")}}</label>
											<input id="infor-m3-viewer-bookmark-option" [disabled]="!ioSettings.enableBookmarks" class="input-xs" [(ngModel)]="ioSettings.bookmarkData.option"/>
										</div>
									</div>
									<div class="one-third column">
										<div class="field">
											<label for="infor-m3-viewer-bookmark-panel">{{translate("startPanel")}}</label>
											<input id="infor-m3-viewer-bookmark-panel" [disabled]="!ioSettings.enableBookmarks" class="input-xs" [(ngModel)]="ioSettings.bookmarkData.startPanel"/>
										</div>
									</div>
									<div class="one-third column">
										<div class="field">
											<label for="infor-m3-viewer-bookmark-focus">{{translate("focusField")}}</label>
											<input id="infor-m3-viewer-bookmark-focus" [disabled]="!ioSettings.enableBookmarks" class="input-xs" [(ngModel)]="ioSettings.bookmarkData.focusField"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-buttonset">
			<button class="btn-modal" (click)="onCancel()">{{lang.cancel}}</button>
			<button class="btn-modal-primary" (click)="onOk()">{{lang.ok}}</button>
		</div>

	`,

	styles: [`

	`],
	encapsulation: ViewEncapsulation.None
})

export class InputOutputComponent implements AfterViewInit {
	@Input() widgetContext: IWidgetContext2;
	@Input() widgetInstance: IWidgetInstance2;

	@Input() lang: ILanguage;
	//@ViewChild("inputOutputView") inputOutputRef: ViewContainerRef;

	@ViewChild("inforM3ViewerInputDatagrid") inputDataGrid: SohoDataGridComponent;
	@ViewChild("inforM3ViewerOutputDatagrid") outputDataGrid: SohoDataGridComponent;

	dialog: SohoModalDialogRef<InputOutputComponent>;
	ioSettings: IM3ViewerSettings;
	parameter: any;

	private inputFields: any[];
	private outputFields: any[];
	private inputDatagridOptions: any; //IDataGridOptions;
	private outputDatagridOptions: any; //IDataGridOptions;
	private tabOptions: any; //ITabsOptions;
	//private lang: ILanguage;
	private requestParams: { program: string, transaction: string };
	private recordCounts: number[] = [5, 10, 15, 30, 50, 100];
	private isDrilldown: boolean;
	private isBookmarkSupported: boolean;
	private useBookmark: boolean;
	private isBusy: boolean;
	private modal: any; //JQuery;
	private tab: string;
	private noInputFieldErrorMsg: string;

	constructor(private changeDetection: ChangeDetectorRef, private dialogService: DialogService, private sohoDialogService: SohoModalDialogService) {

	}

	ngOnInit() {
		if (!this.ioSettings.bookmarkData) {
			this.ioSettings.bookmarkData = {};
		}
		this.isDrilldown = this.parameter.isDrilldown ? true : false;
		this.requestParams = {
			program: this.ioSettings.program,
			transaction: this.ioSettings.transaction
		};
		this.tab = this.parameter.type;

		this.tabOptions = {
			modalId: "m3-viewer-io-modal-id",
			tabToSelect: this.tab === "output" ? "#infor-m3-viewer-output-tab" : undefined
		};

		this.initInputGrid();
		this.initOutputGrid();
	}

	ngAfterViewInit() {
		//console.log("dialog-ngAfterViewInit");
		this.load();
	}

	onOk(): void {
		const ioSettings = this.ioSettings;
		if (this.inputFields) {
			ioSettings.input = [];
			for (const field of this.inputFields) {
				if (field.input) {
					ioSettings.input.push({
						field: field.FLNM,
						description: field.FLDS,
						value: field.value.trim(),
						userInput: field.userInput,
						entityType: field.entityType // TODO CONTEXT APP ADDITION
					});
				}
			}
		}

		if (this.outputFields) {
			ioSettings.output = [];
			for (const field of this.outputFields) {
				if (field.output) {
					ioSettings.output.push({
						field: field.FLNM,
						description: field.FLDS,
						type: field.TYPE,
						// tslint:disable-next-line:radix
						order: parseInt(field.order)
					});
				}
			}
		}

		if (ioSettings.bookmarkData.focusField) {
			ioSettings.bookmarkData.focusField = ioSettings.bookmarkData.focusField.toUpperCase();
		}
		if (ioSettings.bookmarkData.startPanel) {
			ioSettings.bookmarkData.startPanel = ioSettings.bookmarkData.startPanel.toUpperCase();
		}

		const result: IDialogResult = {
			value: ioSettings
		};

		this.dialog.close(result);
	}

	onCancel(): void {
		this.dialog.close();
	}

	private load() {

		this.getInputOutputFields("I");
		this.getInputOutputFields("O");

		this.dialog.afterOpen((arg: any[]) => {
			const dialog = this.dialog as any;
			dialog.modal.resize();
		});
	}

	private adjustModal(): void {
		if (this.modal) {
			setTimeout(() => {
				this.modal.data("modal").resize();
			}, 10);
		}
	}

	private getInputOutputFields(type: string, onTabChange?: boolean): void {

		const requestOptions = {
			program: "entities",
			transaction: "LstIFields",
			maxReturnedRecords: 0,
			outputFields: ["FLNM", "FLDS", "TYPE", "MAND"],
			record: {
				MINM: this.ioSettings.program,
				TRNM: this.ioSettings.transaction,
				TRTP: type
			}
		};


		this.isBusy = true;

		//this.service.listMiData(requestOptions).then((items) => {
		TransactionService.listMiData(requestOptions).subscribe((items: any) => {

			const isInput = type === "I";
			isInput ? this.inputFields = items as any : this.outputFields = items as any;
			if (items && items.length) {
				if (isInput) {
					this.extendInputFields();
					this.inputDatagridOptions.dataset = this.inputFields;
					this.inputDataGrid.gridOptions = this.inputDatagridOptions;

				} else {
					this.extendOutputFields();
					this.outputDatagridOptions.dataset = this.outputFields;
					this.outputDataGrid.gridOptions = this.outputDatagridOptions;

				}
			}
			if (!isInput) {
				this.isBookmarkSupported = this.checkBookmarkSupport();
				if (this.isBookmarkSupported) {
					this.ioSettings.bookmarkType = ArrayUtil.containsByProperty(this.outputFields, "FLNM", BookmarkSupport.PROGRAM_NAME) ? BookmarkType.Old : BookmarkType.New;
				}
			}
			if (onTabChange) {
				this.adjustModal();
			}
			this.isBusy = false;
			//this.changeDetection.detectChanges();
		}, (errorResponse: any) => {
			this.isBusy = false;
			if (errorResponse.errorCode === "CR43303") {
				this.noInputFieldErrorMsg = errorResponse.errorMessage;
			} else {
				this.dialogService.showMessage({ isError: true, title: this.translate("requestFailed"), message: errorResponse.errorMessage });
			}
		});
	}

	private checkBookmarkSupport(): boolean {
		const supported = ArrayUtil.containsByProperty(this.outputFields, "FLNM", BookmarkSupport.KEY_STRING)
			&& (ArrayUtil.containsByProperty(this.outputFields, "FLNM", BookmarkSupport.PROGRAM_NAME) || ArrayUtil.containsByProperty(this.outputFields, "FLNM", BookmarkSupport.PROGRAM_NAME_2))
			&& (ArrayUtil.containsByProperty(this.outputFields, "FLNM", BookmarkSupport.TABLE_NAME) || ArrayUtil.containsByProperty(this.outputFields, "FLNM", BookmarkSupport.TABLE_NAME_2));
		if (!supported) {
			this.ioSettings.enableBookmarks = false;
		}
		return supported;
	}

	private extendOutputFields(): void {
		for (const field of this.outputFields) {
			const inSettings = ArrayUtil.itemByProperty(this.ioSettings.output, "field", field.FLNM);
			if (inSettings) {
				field.output = true;
				field.order = inSettings.order;
			} else {
				field.output = false;
			}

			if (field.TYPE === "A") {
				field.displayType = this.translate("string");
			} else if (field.TYPE === "D") {
				field.displayType = this.translate("date");
			} else {
				field.displayType = this.translate("numeric");
			}
		}
	}

	private extendInputFields(): void {
		for (const field of this.inputFields) {
			const inSettings = ArrayUtil.itemByProperty(this.ioSettings.input, "field", field.FLNM);
			field.value = "";
			field.userInput = false;
			field.entityType = ""; // TODO CONTEXT APP ADDITION
			if (inSettings) {
				field.input = true;
				if (inSettings.value.length) {
					field.value = inSettings.value;
				}
				// TODO CONTEXT APP ADDITION
				if (inSettings.entityType && inSettings.entityType.length) {
					field.entityType = inSettings.entityType;
				}
				//
				if (inSettings.userInput) {
					field.userInput = true;
				}
			} else {
				field.input = false;
			}
			if (field.MAND === "1") {
				field.MAND = this.lang.yes.toUpperCase();
				field.input = true;
			} else {
				field.MAND = "";
			}
			if (field.TYPE === "A") {
				field.TYPE = this.translate("string");
			} else if (field.TYPE === "D") {
				field.TYPE = this.translate("date");
			} else {
				field.TYPE = this.translate("numeric");
			}
		}
	}

	private initInputGrid(): void {

		const columns: SohoDataGridColumn[] = [];

		columns.push({ id: "input", name: this.translate("input"), field: "input", sortable: false, align: "center", formatter: (window as any).Formatters.Checkbox, editor: (window as any).Editors.Checkbox });
		columns.push({ id: "FLNM", name: this.translate("name"), field: "FLNM", sortable: false, formatter: (window as any).Formatters.Readonly });
		columns.push({ id: "FLDS", name: this.translate("description"), field: "FLDS", sortable: false, formatter: (window as any).Formatters.Readonly });
		columns.push({ id: "value", name: this.translate("value"), field: "value", sortable: false, editor: (window as any).Editors.Input });
		columns.push({ id: "userInput", name: this.translate("userInput"), field: "userInput", sortable: false, align: "center", formatter: (window as any).Formatters.Checkbox, editor: (window as any).Editors.Checkbox });
			
		columns.push({ id: "MAND", name: this.translate("mandatory"), field: "MAND", sortable: false, formatter: (window as any).Formatters.Readonly });
		columns.push({ id: "TYPE", name: this.translate("type"), field: "TYPE", sortable: false, formatter: (window as any).Formatters.Readonly });

		this.inputDatagridOptions = {
			columns: columns,
			dataset: [],
			editable: true,
			rowHeight: "short"
		};
	}

	private initOutputGrid(): void {
		const columns: any = [
			{ id: "output", name: this.translate("output"), field: "output", sortable: false, align: "center", formatter: (window as any).Formatters.Checkbox, editor: (window as any).Editors.Checkbox },
			{ id: "FLNM", name: this.translate("name"), field: "FLNM", sortable: false, formatter: (window as any).Formatters.Readonly },
			{ id: "FLDS", name: this.translate("description"), field: "FLDS", sortable: false, formatter: (window as any).Formatters.Readonly },
			// { id: "TYPE", name: this.translate("type"), field: "displayType", sortable: false, formatter: (window as any).Formatters.Readonly },
			{ id: "order", name: this.translate("viewOrder"), field: "order", sortable: false, editor: (window as any).Editors.Input, mask: "##", maskMode: "number" }
		];

		this.outputDatagridOptions = {
			columns: columns,
			dataset: [],
			editable: true,
			rowHeight: "short"
		};
	}

	private onEnableBookmarksChange(): void {
		if (this.ioSettings.enableBookmarks) {
			this.ioSettings.bookmarkData.option = "5";
		} else {
			this.ioSettings.bookmarkData = {};
		}
	}

	private translate(text: string): string {
		return this.lang.get(text);
	}

}
