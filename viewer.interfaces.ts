﻿import {
	IMIRequest
} from "./angular-common";

import {
	IDialogOptions,
	ILanguage,
} from "lime";

import {
	IBookmark as m3IBookmark
} from "./angular-common";

export interface IM3ViewerDataset {
	program: string;
	transaction: string;
	input: IInput[];
	output?: IOutput[];
	displayType?: DataDisplayType;
	chartSettings?: IChartSettings;
	maxRecords: number;
	enableBookmarks?: boolean;
	bookmarkType?: BookmarkType;
	bookmarkData?: IBookmarkData;
	page?: string;
}

export enum BookmarkType {
	Old = 0,
	New = 1
}

export interface IM3ViewerSettings extends IM3ViewerDataset {
	refreshRate: number;
}

export interface IBookmark {
	isFolder?: boolean;
	items?: IBookmark[];
	name?: string;
	url?: string;
}

export enum PrimaryAction {
	Drilldown = 0,
	OpenBookmark = 1
}

export interface IMiBrowseDialogOptions extends IDialogOptions {
	parameter: { lang: ILanguage, browseOptions: IMIRequest, cache?: boolean };
}

export interface IM3MonitorDataset extends IM3ViewerDataset {
	name: string;
	id: string;
	primaryAction: PrimaryAction;
	enableDrilldown: boolean;
	enableColors: boolean;
	colorSchemes?: { [key: number]: IMonitorColorScheme };
	maxMonitorRecords: number;
	bookmarkOptions: m3IBookmark;
}

export interface IMonitorColorScheme {
	lowerThan?: number;
	higherThan?: number;
}

export enum ColorScheme {
	Info = 0,
	Good = 1,
	Warning = 2,
	Error = 3
}

export interface IM3MonitorSettings {
	refreshRate: number;
	monitors?: IM3MonitorDataset[];
}

export interface IBookmarkData {
	option?: string;
	startPanel?: string;
	focusField?: string;
}

export interface IOutput {
	field: string;
	description: string;
	type?: string;
	order?: number;
}

export interface IInput {
	field: string;
	description?: string;
	value: string;
	userInput?: boolean;
	entityType?: string; // TODO CONTEXT APP ADDED PROPERTY
}

export enum DataDisplayType {
	List = 0,
	Card = 1,
	Graph = 2
}

export interface IChartType {
	value: ChartType;
	label: string;
}

export interface IGroupType {
	value: GroupType;
	label: string;
}

export enum ChartType {
	Pie = 0,
	Bar = 1,
	Donut = 2,
	Column = 3,
	Line = 4,
	Area = 5
}

export enum GroupType {
	Count = 0,
	Sum = 1
}

export interface IChartSettings {
	chartType: ChartType;
	labelColumn?: string;
	valueColumns?: string[];
	groupByType?: GroupType;
	groupByLabel?: boolean;
	enableConversion?: boolean;
	debit?: string;
	credit?: string;
	drilldownSettings?: IM3ViewerDataset;
}

export class BookmarkSupport {
	static PROGRAM_NAME = "YPGM";
	static PROGRAM_NAME_2 = "PGNM";
	static TABLE_NAME = "YFIL";
	static TABLE_NAME_2 = "FILE";
	static KEY_STRING = "KSTR";
}

/**
 * Refresh interval for data reload (in ms).
 */
export class RefreshIntervals {
	static NO_REFRESH = 0;
	static FIFTEEN_MIN = 15;
	static THIRTY_MIN = 30;
	static FORTYFIVE_MIN = 45;
	static SIXTY_MIN = 60;
}
