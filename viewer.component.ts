import {
    CommonModule
} from "@angular/common";

import {
    FormsModule
} from "@angular/forms";

import {
    AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Injectable, Input,
    NgModule, OnDestroy, QueryList, Renderer2, ViewChild, ViewChildren,
    ViewContainerRef, ViewEncapsulation
} from "@angular/core";

import {
    ExpandableAreaComponent,
    SohoBusyIndicatorModule, SohoButtonModule, SohoChartModule, SohoContextMenuModule,
    SohoDataGridComponent, SohoDataGridModule, SohoDropDownModule, SohoExpandableAreaModule, SohoIconModule, SohoInputModule,
    SohoLabelModule, SohoListViewModule, SohoModalDialogModule, SohoModalDialogService, SohoPieModule, SohoPopupMenuModule,
    SohoSearchFieldComponent,
    SohoSearchFieldModule, SohoTabsModule, SohoToastService, SohoTooltipDirective, SohoTreeComponent,
} from "@infor/sohoxi-angular";

import {
    ArrayUtil, CommonUtil as lmCommonUtil, DialogButtonType, DialogService, IDialogOptions,
    IDialogResult, ILanguage, ILaunchOptions, IWidgetAction, IWidgetComponent, IWidgetContext2,
    IWidgetInstance2, IWidgetMessage, IWidgetSettingsComponent, IWidgetSettingsContext2, IWidgetSettingsInstance2,
    Log, NumUtil, StandardDialogButtons, WidgetMessageType, WidgetState
} from "lime";

import {
    Subject
} from "rxjs/Subject";

import {
    AsyncSubject
} from "rxjs/AsyncSubject";

import {
    Observable
} from "rxjs/Observable";

import {
    Bookmark, IBookmark, IMIRequest, IMIResponse, IMIService, IUserContext,
    AngularCommon, MIUtil
} from "./angular-common";

import {
    BookmarkSupport, BookmarkType, ChartType, DataDisplayType, GroupType, IInput,
    IM3ViewerSettings, IOutput, RefreshIntervals
} from "./viewer.interfaces";

import {
    M3CommonUtil
} from "./util";

import {
    TransactionComponent
} from "./transaction.component";

import {
    BrowseDialogComponent
} from "./browser.dialog.component";

import {
    InputOutputComponent
} from "./input.output.component";

import {
    UserInputComponent
} from "./set.user.input.component";
import { iterateListLike } from "@angular/core/src/change_detection/change_detection_util";

import {
    SCREENLIST
} from "./screenid"

enum RequestMode {
    Normal = 0,
    Drilldown = 1,
    UserInput = 2
}

@Component({
    selector: "m3-viewer",
    template: `
        <div class="lm-width-full lm-height-full" style="display:flex; flex-flow:column">
            <div style="flex:0 1 auto;">
                <soho-expandable-area #expandableArea [closed]="true" class="expandableSearch"
                    (collapse)="isSearchAreaExpanded = false"
                    (expand)="isSearchAreaExpanded = true">
                    <soho-expandable-pane >
                        <input class="searchInput" soho-searchfield class="lm-width-full"
                            placeholder="Search"
                            [(ngModel)]="searchInput"
                            [clearable]="true"
                            (ngModelChange)="search()"
                            (keyup.esc)="clearSearch()"
                        />
                    </soho-expandable-pane>
                </soho-expandable-area>
            </div>
            <div *ngIf="settings" [ngClass]="{'chartContainer': settings.displayType === dataDisplayType.Graph, 'listAndGridContainer': settings.displayType !== dataDisplayType.Graph}">
                    <div *ngIf="settings.displayType === dataDisplayType.List" [ngClass]="{'lm-height-full' : true, 'gridContainerWithSearch' : isSearchAreaExpanded, 'gridContainerWithoutSearch' : !isSearchAreaExpanded}" >
                        <div #viewListDatagrid soho-datagrid [hidden]="!(dataGridOptions?.dataset?.length)"
                            class="{{datagridClass}} datagrid-container lm-scroll-no-x"
                            [gridOptions] = "dataGridOptions"
                            [menuId]="datagridMenuId" (contextMenu)="onContextMenu($event)"
                            (selected)="onSelected($event)"
                            (rowDoubleClicked)="onDoubleClick($event)">
                        </div>

                        <div *ngIf="settings.enableBookmarks" class="popupmenu-wrapper" role="application" aria-hidden="true">
                            <ul soho-popupmenu id="{{datagridMenuId}}" menu="{{datagridMenuId}}" trigger='immediate'
                                [eventObj]="contextMenuEvent" [attachToBody]="true">

                                <li><a href="#" (click)="openBookmark()">{{translate("open")}}</a></li>
                                <li><a href="#" (click)="copyToClipboard()">{{translate("copyToClipboard")}}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div *ngIf="settings.displayType === dataDisplayType.Card" class="listContainer">
                        <div *ngIf="viewDrilldown" style="border-bottom: 1px solid #d8d8d8;padding:10px">
                            <button [ngClass]="{ 'btn-tertiary': isMonitor, 'btn-icon': !isMonitor }" (click)="onExitDrilldown()">
                                <span class="audible">{{translate("goBack")}}</span>
                                <svg soho-icon icon="left-arrow" class="icon" focusable="false">

                                </svg>
                                <span *ngIf="isMonitor">{{settings.name}}</span>
                            </button>
                        </div>
                        <div class="listview" style="height: auto;">
                            <soho-listview #singleSelectListView class="cardListView">
                                <li soho-listview-item *ngFor="let item of cardsData" soho-context-menu menu="m3AppMsgCard_{{widgetId}}" (beforeopen)="selectedItem=item"
                                    (dblclick)="onDoubleClick(item)" (click)="onSelected(item)">
                                    <table class="listview-subheading">
                                        <ng-container *ngFor="let field of item">
                                            <tr *ngIf="field.visible">
                                                <td>
                                                    <div>
                                                        <strong>{{field.fieldDisplayName}}</strong>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>{{field.value}}</div>
                                                </td>
                                            </tr>
                                        </ng-container>
                                    </table>
                                </li>
                            </soho-listview>
                            <!-- Context menu -->
                            <ul soho-popupmenu id="m3AppMsgCard_{{widgetId}}" *ngIf="settings.enableBookmarks || (viewDrilldown && settings.chartSettings.drilldownSettings.enableBookmarks) ">
                                <li><a href="#" (click)="openBookmark(selectedItem)">{{translate('open')}}</a></li>
                                <li><a href="#" (click)="copyToClipboard(selectedItem)">{{translate('copyToClipboard')}}</a></li>
                            </ul>
                        </div>
                    </div>

                    <div #transactionDialog>
                    </div>

            </div>
        </div>

    `,
    styles: [`

        m3-viewer soho-expandable-area.expandableSearch > .expandable-area > .expandable-footer {
            display: none;
        }


        m3-viewer .expandable-area .expandable-pane > .content {
            padding: 16px !important;
        }

        m3-viewer [hidden]:not([broken]) {
            display: none !important;
        }

        m3-viewer .expandable-area .searchfield-wrapper{
            margin-bottom: 0px;
            width: 100%;
        }

        m3-viewer soho-expandable-pane > input.searchInput {
            width: 100%
        }

        m3-viewer .chartContainer {
            height: 100%;
        }

        m3-viewer .listAndGridContainer {
            flex: 1 1 0px;
            overflow-x: auto;
            padding: 0px !important;
        }

        m3-viewer .gridContainerWithSearch {
            padding: 0px 16px 16px 16px !important;
        }

        m3-viewer .gridContainerWithoutSearch {
            padding: 16px !important;
        }

        m3-viewer .listContainer {
            padding: 0px 0px 0px 0px !important;
        }

        m3-viewer tr td:last-child {
            padding-left: 10px;
        }

        m3-viewer .listview soho-listview.cardListView tr.is-selected td:first-child {
            padding: 0 0 3px 0;
        }

        m3-viewer .listview soho-listview.cardListView tr.is-selected,
        m3-viewer .listview soho-listview.cardListView tr.is-selected:hover {
            background: transparent;
        }

        m3-viewer .listview soho-listview.cardListView  td {
            padding-bottom: 3px;
        }
	`],

    //changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
export class ViewerComponent implements AfterViewInit, IWidgetComponent {
    @Input() widgetContext: IWidgetContext2;
    @Input() widgetInstance: IWidgetInstance2;

    @Input() isMonitor: boolean = false;
    @Input() miService: IMIService;
    @Input() settings: IM3ViewerSettings;
    @ViewChild(ExpandableAreaComponent) expandableArea: ExpandableAreaComponent;
    @ViewChild(SohoSearchFieldComponent) searchElement: any;
    @ViewChild(SohoDataGridComponent) datagrid: SohoDataGridComponent;
    @ViewChild("transactionDialog", { read: ViewContainerRef }) transactionDialog: ViewContainerRef;

    @ViewChildren(SohoTooltipDirective) tooltips: QueryList<SohoTooltipDirective>;
    items: IMIResponse[];
    searchInput = "";
    isSearchAreaExpanded = false;

    private dataDisplayType = DataDisplayType;

    private displayType: string;
    private templateUrl: string;
    private drilldownItem: any;
    private dataGridOptions: SohoDataGridOptions = {
        dataset: [],
        columns: []
    };
    private drilldownCardsData: any[] = [];
    private chartOptions: SohoChartOptions = {
        type: "pie",
        dataset: undefined
    };
    private cardsData: any = [];
    private rawData: any[] = [];
    private drillRawData: any[] = [];
    private filteredData: any[] = [];
    private viewDrilldown = false;
    private userInput: IInput[];

    private datagridClass = "m3InfoViewDataGrid";
    private datagridMenuId = "m3InfoViewDataGridMenu";
    private selectedItemInDataGrid: any;
    private requestParameters: string[];
    //private isMonitor: boolean;

	/**
	 * Widget instance ID.
	 */
    private widgetId: string;

    //private miService: IMIService;
    private logPrefix = "[M3 Viewer Widget] ";
    private orderBy: string;
    private refreshTimeoutId: number;
    private userContext: IUserContext;

    private searchLimit = 50;
    private instanceId: string;
    private searchActive: boolean;
    private searchCompleted: boolean;
    private lang: ILanguage;

    private dataIsInvalid = false;

    private hasSearchHits = true;

    private drillbackPrefixUrl = "?LogicalId=lid://infor.shub.shub";

    private selectedItemRecord: any = null;

    constructor(private changeDetection: ChangeDetectorRef, private dialogService: DialogService, private sohoDialogService: SohoModalDialogService) {

    }

    ngOnInit() {
        this.lang = this.widgetContext.getLanguage();
        this.widgetId = this.widgetContext.getWidgetInstanceId();
        this.datagridMenuId = "m3InfoViewDataGridMenu_" + this.widgetContext.getWidgetInstanceId();
    }

    ngAfterViewInit() {
        try {
            this.setBusy(true);
            this.miService.getUserContext().subscribe((userContext: IUserContext) => {
                this.userContext = userContext;
                if (this.isMonitor) {
                    this.initializeMonitorViewer();
                } else {
                    this.initialize();
                }
            }, (error) => {
                if (error.errorMessage) {
                    const message: IWidgetMessage = {
                        type: WidgetMessageType.Error,
                        message: error.errorMessage
                    };
                    this.widgetContext.showWidgetMessage(message);
                }
                this.setBusy(false);
            });

            if (!this.isMonitor) {
                this.widgetInstance.deactivated = () => {
                    if (this.refreshTimeoutId) {
                        window.clearTimeout(this.refreshTimeoutId);
                    }
                };

                this.widgetInstance.activated = () => {
                    this.startRefreshTimer();
                };
            }

            // workaround for the search field clear not triggering the ngModelChange
            this.searchElement.jQueryElement.on("cleared", () => {
                this.clearSearch();
            });
        } catch (error) {
            this.showErrorMessage("", error);
        }
    }

	/**
	 * Clear the search field and the filtered data.
	 */
    clearSearch(): void {
        this.searchInput = "";
        this.search();
    }

    /**
     * Search the data and filter out items with no search hits.
     */
    search(): void {
        this.filteredData = [];
        const raw = this.viewDrilldown ? this.drillRawData : this.rawData;
        if (this.searchInput.length > 0) {
            for (const item of raw) {
                let searchHit = false;
                for (const property in item) {
                    if (property !== "metadata" && item.hasOwnProperty(property)) {
                        if (item[property].toLowerCase().indexOf(this.searchInput.toLowerCase()) > -1) {
                            searchHit = true;
                        }
                    }
                }
                if (searchHit) {
                    this.hasSearchHits = true;
                    this.filteredData.push(item);
                } else {
                    this.hasSearchHits = false;
                }
            }
            if (this.viewDrilldown) {
                this.cardsData = this.parseToCardsData(this.filteredData, [], true);
            } else {
                this.parseData(this.filteredData);
            }
        } else {
            this.filteredData = [];
            this.hasSearchHits = true;
            if (this.viewDrilldown) {
                this.cardsData = this.parseToCardsData(raw, [], true);
            } else {
                this.parseData(raw);
            }
        }
    }

    private setBusy(isBusy?: boolean): void {
        this.widgetContext.setState(isBusy ? WidgetState.busy : WidgetState.running);
    }

    private initialize() {

        // Add search to actions.
        $.extend(this.widgetInstance.actions[0], {
            execute: () => { this.toggleSearch(); },
            text: this.lang.get("search"),
            isEnabled: !this.dataIsInvalid
        });

        // Get settings
        const savedSettings = this.widgetContext.getSettings().getValues() ? $.extend({}, this.widgetContext.getSettings().getValues()) : undefined;

        // Initialize widget if settings exists otherwise show configure message.
        if (!lmCommonUtil.isUndefined(savedSettings.displayType)) {
            this.settings = $.extend({}, savedSettings);
            this.getData(RequestMode.Normal).subscribe((data) => {
                this.rawData = data;
                this.parseData(this.rawData);
                this.startRefreshTimer();
                this.setBusy(false);
            }, (error) => {
                this.setBusy(false);
            });
        } else {
            this.notifyNotConfigured();
            this.setBusy(false);
        }

        this.widgetInstance.settingsSaved = (settingsArg) => this.onSettingsSaved(settingsArg);

        // Add refesh to actions
        $.extend(this.widgetInstance.actions[1], {
            execute: () => { this.onSettingsSaved(this.settings, true); },
            text: this.lang.get("refresh")
        });

        // Add user input to actions.
        $.extend(this.widgetInstance.actions[2], {
            execute: () => { this.setUserInput(); },
            text: this.lang.get("changeInput"),
            isEnabled: this.isUserInputEnabled()
        });

        // Add user input to actions.
        $.extend(this.widgetInstance.actions[3], {
            execute: () => {
                ///alert(1234);
                const resolve = false;
                let drillbackURL = this.drillbackPrefixUrl + "&stateName=customers-list&stateParams={\"#\":null}";
                if (this.selectedItemRecord !== null) {
                    // tslint:disable-next-line:no-console
                    const items = this.selectedItemRecord;

                    drillbackURL = this.drillbackPrefixUrl + "&stateName=product&stateParams={\"#\":null}";

                    // const extendsettings = $.extend(this.settings, {});
                    const extendsettings: any = {};
                    $.extend(extendsettings, this.settings);
                    extendsettings.page = "new-order";
                    drillbackURL = this.getDrillBackUrl(items, extendsettings);

                    this.widgetContext.launch({ url: drillbackURL, resolve: resolve });

                } else {
                    this.widgetContext.launch({ url: drillbackURL, resolve: resolve });
                }

            },
            text: "New Order",
            isEnabled: true
        });

        this.registerContextHandler(); // TODO CONTEXT APP ADDITION

        this.widgetInstance.deactivated = () => {
            this.unRegisterContextHandler();
        };

        this.widgetInstance.activated = () => {
            this.registerContextHandler();
        };

    }

    private initializeMonitorViewer(): void {
        this.getData(RequestMode.Normal).subscribe((data) => {
            this.rawData = data;
            this.parseData(this.rawData);
            this.setBusy(false);
        },
            (error) => {
                this.setBusy(false);
            });
    }

    private onSelected(param: any) {
        let processedItem: any = {};

        // tslint:disable-next-line:no-console
        console.log(param);
        if (param) {
            if (this.settings.displayType === this.dataDisplayType.List) {
                param.item = (param.rows.length > 0) ? param.rows[0].data : null;
                processedItem = param.item;
            } else {
                for (const item of param) {
                    processedItem[item.fieldName] = item.value;
                }
            }

            this.selectedItemRecord = processedItem;
        } else {
            this.selectedItemRecord = null;
        }
    }

    private onDoubleClick(param: any) {
        let processedItem: any = {};
        if (this.settings.displayType === this.dataDisplayType.List) {
            processedItem = param.item;
        } else {
            for (const item of param) {
                processedItem[item.fieldName] = item.value;
            }
        }

        this.launchLink(processedItem);
    }

    private launchLink(items: any): void {
        //default drillbackURL
        let drillbackURL: string = this.drillbackPrefixUrl + "&stateName=product&stateParams={\"#\":null}";
        const resolve: boolean = false;

        drillbackURL = this.getDrillBackUrl(items, this.settings);

        if (drillbackURL.length > 0) {
            this.widgetContext.launch({ url: drillbackURL, resolve: resolve });
        }
    }

    private getDrillBackUrl(items: any, settings: IM3ViewerSettings): string {

        const screenlist = SCREENLIST[settings.program];
        const stateName = (settings.page) ? settings.page : "product";

        // tslint:disable-next-line:only-arrow-functions
        const screen = $.map(screenlist, function (n: any, i) {
            if (n.screenId === stateName) { return n; }
        });
        let url: string = this.drillbackPrefixUrl + "&stateName=product&stateParams={\"#\":null}";
        if (screen.length > 0) {
            let stateParams: string = "";

            const params: any = screen[0].params;
            params.forEach((element: any) => {
                const respKey = Object.keys(element)[0];
                stateParams = this.addStateParameter(stateParams, element[respKey], items[respKey]);
            });

            stateParams = "{" + stateParams + "}";
            url = this.drillbackPrefixUrl
                + "&stateName=" + stateName
                + "&stateParams=" + stateParams;
        }
        return url;
    }

    private addStateParameter(stateParams: any, parameterName: string, parameter: any): string {
        let ret: string = "";

        if (stateParams.length !== 0) ret = stateParams + ",";

        return ret + "\"" + parameterName + "\":\"" + parameter + "\"";
    }

    private showErrorMessage(displayMsg: string, logMsg: string): void {
        Log.debug(this.logPrefix + logMsg);
        this.widgetContext.showWidgetMessage({ type: WidgetMessageType.Error, message: displayMsg });
    }

    private toggleSearch(): void {
        if (!this.searchActive) {
            this.searchActive = true;
            this.expandableArea.toggleOpen(true);
            this.widgetContext.getElement().scrollTop(0);
            this.searchElement.jQueryElement.focus();
        } else {
            this.expandableArea.toggleOpen(false);
            this.searchActive = false;
        }
    }

    private formatTooltip(item: any, groupBy: boolean, value: any, column: any): string {
        const chartSettings = this.settings.chartSettings;
        let tooltip = this.getOutputFieldDescription(chartSettings.labelColumn) + " ";
        if (groupBy) {
            tooltip += item.groupedByValue;
            if (chartSettings.groupByType === GroupType.Count) {
                tooltip += ` ${this.translate("count").toUpperCase()}: ${value}`;
            } else {
                tooltip += `, ${this.getOutputFieldDescription(column)} ${value}`;
            }
        } else {
            tooltip += item[chartSettings.labelColumn] + ", " + this.getOutputFieldDescription(column) + " " + value;
        }
        return tooltip;
    }

    private getOutputFieldDescription(fieldName: string): string {
        const field: IOutput = ArrayUtil.itemByProperty(this.settings.output, "field", fieldName);
        return field ? field.description : fieldName;
    }

	/**
	 * Format values from debit/credit marked to numbers.
	 * @param temp
	 */
    private formatValue(temp: any): number {
        const chartSettings = this.settings.chartSettings;
        if (typeof temp === "string") {
            let cMatcher = "-";
            let dMatcher = "+";
            const last = temp[temp.length - 1];
            if (chartSettings.enableConversion) {
                cMatcher = chartSettings.credit || "-";
                dMatcher = chartSettings.debit || "+";
                if (last === cMatcher || last === dMatcher) {
                    temp = temp.slice(0, - 1);
                    temp = parseFloat(temp);
                }
            } else {
                if (last === cMatcher) {
                    temp = cMatcher.concat(temp.slice(0, - 1));
                    temp = parseFloat(temp);
                } else if (last === dMatcher) {
                    temp = temp.slice(0, - 1);
                    temp = parseFloat(temp);
                }
            }
            // NaN check
            if (isNaN(temp)) {
                this.widgetContext.showWidgetMessage({ type: WidgetMessageType.Error, message: this.lang.get("invalidValues") });
                this.dataIsInvalid = true;
                temp = 0;
            }
        }

        return Math.round(temp * 100) / 100;
    }

    private setUserInput(): void {
        // const options: IDialogOptions = {
        //     title: this.lang.get("changeInput"),
        //     templateUrl: this.getTemplateUrl("common/user-input/user-input.html"),
        //     parameter: { input: this.userInput && this.userInput.length ? this.userInput : this.settings.input, lang: this.lang }
        // }

        const dialog = this.sohoDialogService
            .modal(UserInputComponent, this.transactionDialog)
            .title(this.translate("changeInput"))
            .afterClose((result: IDialogResult) => {
                if (result.button === DialogButtonType.Ok) {
                    if (!this.userInput || JSON.stringify(this.userInput) !== JSON.stringify(result.value)) {
                        this.userInput = result.value;
                        this.getData(RequestMode.UserInput).subscribe((data) => {
                            this.rawData = data;
                            this.parseData(this.rawData);
                            this.search();
                            this.setBusy(false);
                        },
                            (error) => {
                                this.setBusy(false);
                            }
                        );
                    }
                }
            });

        dialog.apply((component) => {
            const userInput = [];
            for (const input of this.settings.input) {
                if (input.userInput) {
                    if (this.userInput && this.userInput.length) {
                        const foundUserInput = this.userInput.find((item) => item.field === input.field);
                        if (foundUserInput) {
                            userInput.push(foundUserInput);
                        } else {
                            userInput.push(input);
                        }
                    } else {
                        userInput.push(input);
                    }
                }
            }
            component.parameter = { input: userInput, lang: this.lang };
            component.dialog = dialog;
        }).open();

    }

	/**
	 * Handles data updates when settings have been saved.
	 * @param saved Saved data object.
	 */
    private onSettingsSaved(saved: any, forceDataRefresh?: boolean): void {
        let newSettings: IM3ViewerSettings;
        if (!forceDataRefresh) {
            newSettings = saved.settings.getValues();
        } else {
            newSettings = saved;
        }

        const settings = this.settings;

        this.widgetContext.removeWidgetMessage();

        if (settings) {
            if (this.viewDrilldown) {
                this.onExitDrilldown();
            }
            const timerUpdated = settings.refreshRate !== newSettings.refreshRate;
            // Get new data if query values have changed.
            if (settings.program !== newSettings.program ||
                settings.transaction !== newSettings.transaction ||
                JSON.stringify(settings.input) !== JSON.stringify(newSettings.input) ||
                JSON.stringify(settings.output) !== JSON.stringify(newSettings.output) ||
                settings.page !== newSettings.page ||
                settings.maxRecords !== newSettings.maxRecords ||
                settings.enableBookmarks !== newSettings.enableBookmarks ||
                forceDataRefresh) {
                this.dataGridOptions.dataset = [];
                this.dataGridOptions.columns = [];
                this.widgetContext.removeWidgetMessage();
                this.settings = forceDataRefresh ? saved : saved.settings.getValues();
                this.getData(RequestMode.Normal).subscribe((items) => {
                    this.rawData = items;
                    if (this.searchInput) {
                        this.parseData(this.filteredData);
                    } else {
                        this.parseData(this.rawData);
                    }
                    this.setSearchAvailability();
                    this.setBusy(false);
                }, (error) => {
                    this.setBusy(false);
                });
            } else {
                // Format existing data and set correct display type
                this.settings = saved.settings.getValues();
                if (this.searchInput) {
                    this.parseData(this.filteredData);
                } else {
                    this.parseData(this.rawData);
                }
                this.setSearchAvailability();
            }

            // Set refresh rate if changed.
            if (timerUpdated) {
                this.startRefreshTimer();
            }

        } else {
            // Get data and update everything
            this.widgetInstance.actions[0].isEnabled = true;
            this.widgetContext.removeWidgetMessage();
            this.settings = saved.settings.getValues();
            this.getData(RequestMode.Normal).subscribe((items) => {
                this.rawData = items;
                this.parseData(this.rawData);
                this.setBusy(false);
            }, (error) => {
                this.setBusy(false);
            });
        }

        this.widgetInstance.actions[2].isEnabled = this.isUserInputEnabled();
    }

	/**
	 * Requests and updates data.
	 * @param mode Request mode.
	 */
    private getData(mode: RequestMode): Observable<any> {
        //const deferred = this.q.defer();
        const subject = new AsyncSubject();
        const settings = this.settings;

        // Do not perform request if mandatory program & transaction not present (!transaction === !program && !transaction)
        if (!settings || !settings.transaction) {
            this.setBusy(false);
            this.setSearchAvailability();
            subject.error("");
            return subject.asObservable();
            //deferred.reject();
            //return deferred.promise;
        }

        this.setBusy(true);
        const chartSettings = settings.chartSettings;
        // Create a string array of output fields.
        const outputFields: any[] = [];
        const output = mode === RequestMode.Drilldown ? chartSettings.drilldownSettings.output : settings.output;

        for (const item of output) {
            outputFields.push(item.field);
        }

        let serviceToPages: any = {};
        serviceToPages = SCREENLIST;
        const pagesArray = serviceToPages[this.settings.program];

        const stateName = this.settings.page ? this.settings.page : "product";
        if (pagesArray) {
            for (const screen of pagesArray) {
                if (stateName === screen["screenId"]) {
                    let screenParams = screen["params"];
                    this.requestParameters = screenParams.map((item: any) => {
                        return Object.keys(item)[0];
                    });
                    break;
                }
            }

            if (this.requestParameters) {
                for (const item of this.requestParameters) {
                    if (outputFields.indexOf(item) < 0) {
                        outputFields.push(item);
                    }
                }
            }
        }

        let request: IMIRequest;

        switch (mode) {
            case RequestMode.Drilldown:
                request = {
                    program: chartSettings.drilldownSettings.program,
                    transaction: chartSettings.drilldownSettings.transaction,
                    maxReturnedRecords: chartSettings.drilldownSettings.maxRecords || 0,
                    record: M3CommonUtil.getInputAsRecord(chartSettings.drilldownSettings.input, this.userContext, this.drilldownItem),
                    outputFields: outputFields
                };
                break;

            case RequestMode.UserInput:
            case RequestMode.Normal:
            default:
                request = {
                    program: settings.program,
                    transaction: settings.transaction,
                    maxReturnedRecords: settings.maxRecords,
                    record: mode === RequestMode.Normal ? M3CommonUtil.getInputAsRecord(settings.input, this.userContext) : M3CommonUtil.getInputAsRecord(this.userInput, this.userContext),
                    outputFields: outputFields
                };
                break;
        }

        this.miService.execute(request).subscribe((response: IMIResponse) => {
            subject.next(response.items);
            subject.complete();
            //deferred.resolve(response.items);
            // Set busy false handled by success callback for getData() users
        }, (errorResponse: IMIResponse) => {
            //deferred.reject(errorResponse);
            subject.error(errorResponse);
            let errorMsg = errorResponse.errorMessage;
            if (!errorMsg || !errorMsg.length) {
                errorMsg = this.translate("requestFailed");
            }
            this.widgetContext.showWidgetMessage({ type: WidgetMessageType.Error, message: errorMsg });
            this.setBusy(false);
        });

        // return deferred.promise;
        return subject.asObservable();
    }

    private parseData(items: any[]): void {
        const outputFields = this.settings.output;

        this.dataIsInvalid = false;

        switch (this.settings.displayType) {
            case DataDisplayType.Card:
                this.cardsData = [];
                this.cardsData = this.parseToCardsData(items, this.requestParameters);
                break;

            case DataDisplayType.List:
                this.dataGridOptions = this.parseToDataGridDataOptions(items);
                break;
        }
        this.setSearchAvailability();
    }

    private getDisplayValue(rawValue: string, fieldType: string): string {
        if (!rawValue) { return rawValue; }
        let displayValue: string;
        switch (fieldType) {
            case "N":
                displayValue = this.formatDisplayNumeric(rawValue);
                break;
            case "D":
                displayValue = this.formatDisplayDate(rawValue);
                break;
            case "A":
            default:
                displayValue = rawValue;
                break;
        }

        return displayValue;
    }

    private formatDisplayDate(raw: string): string {
        if (this.userContext) {
            let formatted: string;
            switch (this.userContext.DTFM) {
                case "DMY":
                    formatted = raw.substr(6, 2) + "/" + raw.substr(4, 2) + "/" + raw.substr(0, 4);
                    break;
                case "MDY":
                    formatted = raw.substr(4, 2) + "/" + raw.substr(6, 2) + "/" + raw.substr(0, 4);
                    break;
                case "YMD":
                default:
                    formatted = raw.substr(0, 4) + "-" + raw.substr(4, 2) + "-" + raw.substr(6, 2);
                    break;
            }
            return formatted;
        } else {
            // Default API format yyyyMMdd with "-" separators
            return raw.substr(0, 4) + "-" + raw.substr(4, 2) + "-" + raw.substr(6, 2);
        }
    }

    private formatDisplayNumeric(raw: string): string {
        let separator: string;
        if (this.userContext) {
            separator = this.userContext.DCFM;
        }
        if (separator) {
            return NumUtil.format(raw, { separator: separator });
        } else {
            return NumUtil.format(raw);
        }
    }

    private sortOutputs(outputs: IOutput[]): IOutput[] {
        return outputs.map((output) => {
            output.order = output.order || 100;
            return output;
        }).sort((a, b) => {
            return a.order - b.order;
        });
    }

	/**
	 * Parses data to fit the card view (xi-listview).
	 * @param items Items to parse.
	 * @param outputFields Out put fields.
	 * @param isDrilldown Indicates if the data is for a drolldwon.
	 */
    private parseToCardsData(items: any[], requestParameters: string[], isDrilldown?: boolean): any[] {
        const cardsData: any[] = [];
        let outputFields = isDrilldown ? this.settings.chartSettings.drilldownSettings.output : this.settings.output;

        if (outputFields.length) {
            outputFields = this.sortOutputs(outputFields);
            for (const item of items) {
                const end: any[] = [];

                for (const field of outputFields) {
                    const endObj = {
                        fieldName: field.field,
                        fieldDisplayName: field.description,
                        value: this.getDisplayValue(item[field.field], field.type),
                        visible: true
                    };
                    end.push(endObj);
                }

                if (requestParameters) {
                    for (const field of requestParameters) {
                        const endObj = {
                            fieldName: field,
                            fieldDisplayName: "",
                            value: item[field],
                            visible: false
                        };

                        end.push(endObj);
                    }
                }

                cardsData.push(end);
            }
        }
        return cardsData;
    }

	/**
	 * Parse data to fit the grid view (xi-datagrid).
	 * @param items Items to parse.
	 */
    private parseToDataGridDataOptions(items: any[]): SohoDataGridOptions {
        const options: SohoDataGridOptions = { dataset: [], columns: [] };

        const columns: any[] = [];
        const gridItems = $.extend([], items);
        // Create column headers
        let outputs = this.settings.output;

        if (outputs.length) {
            outputs = this.sortOutputs(outputs);
            for (const output of outputs) {
                columns.push({
                    id: output.field,
                    name: output.description,
                    field: output.field
                });
            }

            options.columns = columns;
            options.menuId = this.datagridMenuId;
            options.saveColumns = false;
            options.rowHeight = "short";
            options.selectable = "single";

            // Create grid data
            for (const item of gridItems) {
                delete item.metadata;

                // Convert to user format.
                for (const i in item) {
                    // Check if it's a bookmark field and it's not supposed to be displayed in view, if not no display value is fetched
                    if (ArrayUtil.containsByProperty(outputs, "field", i)) {
                        item[i] = this.getDisplayValue(item[i], ArrayUtil.itemByProperty(outputs, "field", i).type);
                    }
                }

                options.dataset.push(item);
            }
        }

        return options;
    }

    private notifyNotConfigured(): void {
        const message: IWidgetMessage = {
            type: WidgetMessageType.Info,
            message: this.lang.get("widgetNotConfigured")
        };
        this.widgetContext.showWidgetMessage(message);
        this.widgetInstance.actions[0].isEnabled = false;
    }

    /**
     * Indicates if user input is enabled.
     */
    private isUserInputEnabled(): boolean {
        if (this.settings && this.settings.input.length) {
            for (let i = 0; i < this.settings.input.length; i++) {
                if (this.settings.input[i].userInput) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }

	/**
	 * Handle exit of drilldown (go back to first view).
	 */
    private onExitDrilldown(): void {
        this.clearSearch();
        this.viewDrilldown = false;
    }

    private unRegisterContextHandler(): void {
        infor.companyon.client.unRegisterMessageHandler("inforBusinessContext");
    }

    private registerContextHandler(): void {
        infor.companyon.client.registerMessageHandler("inforBusinessContext", (data: any) => {
            if (this.settings) {
                this.userInput = [];
                const entities = data.entities;
                for (const input of this.settings.input) {
                    const matchingEntity = ArrayUtil.itemByProperty(entities, "entityType", input.entityType);
                    if (matchingEntity) {
                        this.userInput.push({ field: input.field, value: matchingEntity.id1 });
                    }
                }
                if (this.userInput.length) {
                    this.getData(RequestMode.UserInput).subscribe((items) => {
                        this.rawData = items;
                        if (this.searchInput) {
                            this.parseData(this.filteredData);
                        } else {
                            this.parseData(this.rawData);
                        }
                        this.setSearchAvailability();
                        this.setBusy(false);
                    }, (error) => {
                        this.setBusy(false);
                    });
                }
            }
        });
    }

	/**
	 * Create bookmark launch configuration.
	 * @param item
	 */
    private createBookmarkLaunchConfig(item: any): ILaunchOptions {
        const bookmark: IBookmark = {};
        const prefix = "?LogicalId={logicalId}&";

        // Get item if not provided (i.e. using datagrid)
        if (!item) {
            item = this.selectedItemInDataGrid;
        }

        const programField = this.settings.bookmarkType === BookmarkType.New ? BookmarkSupport.PROGRAM_NAME_2 : BookmarkSupport.PROGRAM_NAME;
        const tableField = this.settings.bookmarkType === BookmarkType.New ? BookmarkSupport.TABLE_NAME_2 : BookmarkSupport.TABLE_NAME;

        // Create keys string and set program and table.
        if (this.templateUrl.indexOf("viewer-list.html") !== -1) {
            bookmark.keys = item[BookmarkSupport.KEY_STRING];
            bookmark.program = item[programField];
            bookmark.table = item[tableField];
        } else if (this.templateUrl.indexOf("viewer-cards.html") !== -1) {
            bookmark.keys = ArrayUtil.itemByProperty(item, "fieldName", BookmarkSupport.KEY_STRING).value;
            bookmark.program = ArrayUtil.itemByProperty(item, "fieldName", programField).value;
            bookmark.table = ArrayUtil.itemByProperty(item, "fieldName", tableField).value;
        }

        // Add additional data to bookmark.
        const bookmarkData = this.viewDrilldown ? this.settings.chartSettings.drilldownSettings.bookmarkData : this.settings.bookmarkData;
        bookmark.panel = bookmarkData.startPanel;
        bookmark.focusFieldName = bookmarkData.focusField;
        bookmark.option = bookmarkData.option;

        // return launch options
        return { url: prefix + Bookmark.toUriParams(bookmark, this.userContext), resolve: true };
    }

	/**
	 * Opens a bookmark.
	 * @param item
	 */
    private openBookmark(item: any) {
        const options = this.createBookmarkLaunchConfig(item);
        this.widgetContext.launch(options);
    }

	/**
	 * Translates text.
	 * @param text Text to translate.
	 * @returns translated text
	 */
    private translate(text: string): string {
        return this.lang.get(text);
    }

	/**
	 * Copies a bookmark URL to the clipboard.
	 * @param item Item to get URL from.
	 * @returns {}
	 */
    private copyToClipboard(item: any): void {
        const options = this.createBookmarkLaunchConfig(item);
        this.widgetContext.resolveAndReplaceAsync(options.url).subscribe((resolved: string) => {
            this.dialogService.copyToClipboard({
                copyData: resolved
            });
        });
    }

	/**
	 * Starts a refresh timer and requests new data when it expires.
	 * @returns {}
	 */
    private startRefreshTimer(): void {
        if (!this.settings) {
            return;
        }
        const rate = this.settings.refreshRate;
        if (rate === RefreshIntervals.NO_REFRESH) {
            Log.debug(this.logPrefix + "Setting refresh timeout to: " + rate + " minutes (no refresh).");
            if (this.refreshTimeoutId) {
                window.clearTimeout(this.refreshTimeoutId);
            }
        } else {
            Log.debug(this.logPrefix + "Setting refresh timeout to: " + rate + " minutes.");
            if (this.refreshTimeoutId) {
                window.clearTimeout(this.refreshTimeoutId);
            }

            this.refreshTimeoutId = window.setTimeout(() => {
                this.getData(RequestMode.Normal).subscribe((data) => {
                    this.rawData = data;
                    this.parseData(this.rawData);
                    this.startRefreshTimer();
                    this.setBusy(false);
                }, () => {
                    this.startRefreshTimer();
                });
            }, rate * 60000);
        }
    }

	/**
	 * Sets search availability. Will enable or disable the search button in the widget header.
	 */
    private setSearchAvailability(): void {
        this.widgetInstance.actions[0].isEnabled = !this.dataIsInvalid && this.rawData.length > 0;
    }

    private onContextMenu(e: SohoDataGridRowClicked) {
        //console.log("contextmenu fired", e);
        this.selectedItemInDataGrid = e.item;
    }
}

@NgModule({
    imports: [CommonModule, SohoBusyIndicatorModule, SohoButtonModule, SohoChartModule, SohoContextMenuModule,
        SohoDataGridModule, SohoDropDownModule, SohoExpandableAreaModule, SohoIconModule, SohoInputModule, SohoLabelModule,
        SohoListViewModule,
        SohoModalDialogModule, SohoPieModule, SohoPopupMenuModule, SohoSearchFieldModule, SohoTabsModule, FormsModule],
    exports: [ViewerComponent, TransactionComponent, BrowseDialogComponent, InputOutputComponent, UserInputComponent],
    declarations: [ViewerComponent, TransactionComponent, BrowseDialogComponent, InputOutputComponent, UserInputComponent],
    providers: [],
    entryComponents: [ViewerComponent, TransactionComponent, BrowseDialogComponent, InputOutputComponent, UserInputComponent]
})
export class ViewerModule {
}
